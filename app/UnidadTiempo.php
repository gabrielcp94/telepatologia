<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UnidadTiempo extends Model
{
    protected $connection = 'mysql';
    protected $table = 'unidad_tiempo';
}