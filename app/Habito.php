<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Habito extends Model
{
    protected $connection = 'mysql';
    protected $table = 'habito';

    use SoftDeletes;

    public $guarded = [];
}