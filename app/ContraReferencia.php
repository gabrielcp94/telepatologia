<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ContraReferencia extends Model
{
    protected $connection = 'mysql';
    protected $table = 'contra_referencia';

    use SoftDeletes;

    public $guarded = [];

    public function referenciaOdontologica()
    {
		return $this->belongsTo('App\ReferenciaOdontologica', 'id_referencia_odontologica');
    }

    public function profesional()
    {
		return $this->belongsTo('App\User', 'created_by');
    }

    public function causalNumero()
    {
		return $this->belongsTo('App\CausalNumero', 'id_causal_numero');
    }

    public function motivoNoPertinente()
    {
		return $this->belongsTo('App\MotivoNoPertinente', 'id_motivo_no_pertinente');
    }
}