<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Auth;

class InformationReceived extends Mailable
{
    use Queueable, SerializesModels;

    public $subject = 'Mensaje de Telepatología.';
    public $request;

    public $distressCall;

    public function __construct($request)
    {
        $this->request = $request;
    }

    public function build()
    {
        return $this->view('information');
    }
}
