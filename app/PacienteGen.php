<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PacienteGen extends Model
{
    protected $connection = 'mysql2';
    protected $table = 'gen_paciente';
}
