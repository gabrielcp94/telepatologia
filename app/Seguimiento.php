<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Seguimiento extends Model
{
    protected $connection = 'mysql';
    protected $table = 'seguimiento';

    use SoftDeletes;

    public $guarded = [];
}