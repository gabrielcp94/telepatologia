<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Osea extends Model
{
    protected $connection = 'mysql';
    protected $table = 'osea';
}