<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Mucosa extends Model
{
    protected $connection = 'mysql';
    protected $table = 'mucosa';
}