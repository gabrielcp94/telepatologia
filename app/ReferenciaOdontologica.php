<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReferenciaOdontologica extends Model
{
    protected $connection = 'mysql';
    protected $table = 'referencia_odontologica';

    use SoftDeletes;

    public $guarded = [];

    public function paciente()
    {
		return $this->belongsTo('App\Paciente', 'id_paciente');
    }

    public function establecimiento()
    {
		return $this->belongsTo('App\Establecimiento', 'id_establecimiento');
    }

    public function profesional()
    {
		return $this->belongsTo('App\User', 'created_by');
    }

    public function cie10()
    {
		return $this->belongsTo('App\Cie10', 'id_cie10');
    }

    public function estado()
    {
		return $this->belongsTo('App\Estado', 'id_estado');
    }

    public function unidad_tiempo()
    {
		return $this->belongsTo('App\UnidadTiempo', 'id_unidad_tiempo');
    }

    public function mucosas()
    {
        return $this->belongsToMany('App\Mucosa', 'referencia_odontologica_mucosa', 'id_referencia_odontologica', 'id_mucosa');
    }

    public function oseas()
    {
        return $this->belongsToMany('App\Osea', 'referencia_odontologica_osea', 'id_referencia_odontologica', 'id_osea');
    }

    public function contraReferencia()
    {
		return $this->hasOne('App\ContraReferencia', 'id_referencia_odontologica');
    }

    public function seguimientos()
    {
        return $this->hasMany('App\Seguimiento', 'id_referencia_odontologica');
    }
}