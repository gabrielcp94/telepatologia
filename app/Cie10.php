<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cie10 extends Model
{
    protected $connection = 'mysql2';
    protected $table = 'gen_diagnostico';

    protected $appends = ['nombre_completo'];

    public function getNombreCompletoAttribute()
    {
        return "{$this->cd_diagnostico} {$this->tx_descripcion}";
    }
}
