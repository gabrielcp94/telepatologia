<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClasificacionFonasa extends Model
{
    protected $connection = 'mysql2';

    protected $table = 'gen_clasificacion_fonasa';
}
