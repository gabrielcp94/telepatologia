<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CausalNumero extends Model
{
    protected $connection = 'mysql';
    protected $table = 'causal_numero';
}