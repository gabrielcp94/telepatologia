<?php

namespace App\Http\Controllers;

use App\Paciente;
use App\PacienteGen;
use Illuminate\Http\Request;
use App\Api\FonasaApi;
use Auth;
use App\Establecimiento;
use Carbon\Carbon;

class PacienteController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $rut = str_replace(".","",request()->rut);
        if(stristr($rut,'-')){
            $rutDV = explode("-",$rut);
            $paciente = Paciente::with('referenciasOdontologicas')->where('nr_run', $rutDV[0])->where('tx_digito_verificador', $rutDV[1])->first();
        }else{
            $paciente = Paciente::with('referenciasOdontologicas')->where('tx_pasaporte', request()->pasaporte)->first();
            if(empty($paciente)){
                $paciente = Paciente::with('referenciasOdontologicas')->where('tx_pasaporte', request()->rut)->first();
            }
        }
        if($paciente){
            return view('indexPaciente', compact('paciente'));
        }else{
            return redirect('referenciaOdontologica')->with('error', "Este Rut/Pasaporte no existe");
        }
    }

    public function getDatosRut(Request $request, FonasaApi $fonasaApi)
    {
        if(isset(request()->pasaporte)){
            $data = PacienteGen::where('tx_pasaporte', request()->pasaporte)->first();
            if(isset($data)){
                $data['encontrado'] = true;
            }else{
                $data['encontrado'] = false;
            }
            return $data;
        }
        $rut = str_replace(".","",request()->rut);
        $rutDV = explode("-", $rut);
        if(isset($rutDV[1]) == false){
            $data['encontrado'] = false;
            return $data;
        }
        $data = $fonasaApi->fetchNormalized($rutDV[0], $rutDV[1]);
        $data['fc_nacimiento'] = date("Y-m-d", strtotime($data['fecha_nacimiento']));
        $paciente = PacienteGen::where('nr_run', $rutDV[0])->where('tx_digito_verificador', $rutDV[1])->first();
            if(isset($paciente)){
                if(!is_null($paciente->tx_direccion)){
                    $data['tx_direccion'] = $paciente->tx_direccion;
                }
                if(!is_null($paciente->tx_telefono)){
                    $data['tx_telefono'] = $paciente->tx_telefono;
                }        
            }
        return $data;
    }

    public function getComunaEstablecimiento(Request $request)
    {
        $comuna = Establecimiento::select('id_comuna')->where('id', request()->id_establecimiento)->first();
        $id_comuna = $comuna->id_comuna;
        return $id_comuna;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Paciente  $paciente
     * @return \Illuminate\Http\Response
     */
    public function show(Paciente $paciente)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Paciente  $paciente
     * @return \Illuminate\Http\Response
     */
    public function edit(Paciente $paciente)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Paciente  $paciente
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Paciente $paciente)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Paciente  $paciente
     * @return \Illuminate\Http\Response
     */
    public function destroy(Paciente $paciente)
    {
        //
    }
}
