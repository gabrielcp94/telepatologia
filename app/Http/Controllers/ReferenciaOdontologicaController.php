<?php

namespace App\Http\Controllers;

use App\ReferenciaOdontologica;
use Illuminate\Http\Request;
use Auth;
use Carbon\Carbon;
use App\Prevision;
use App\ClasificacionFonasa;
use App\Comuna;
use App\Mucosa;
use App\Osea;
use App\Paciente;
use App\Habito;
use App\Cie10;
use App\Establecimiento;
use App\UnidadTiempo;
use App\User;
use App\Seguimiento;

class ReferenciaOdontologicaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(Auth::user()->id_perfil == 3 && !$request->has('id_establecimiento')){
            $request['id_estado'] = 1;
        }
        $referenciasOdontologicas = ReferenciaOdontologica::
                when($request->has('id_establecimiento') && !is_null($request->id_establecimiento), function ($collection) use ($request) {
                    return $collection->where('id_establecimiento', $request->id_establecimiento);
                })
                ->when($request->has('id_profesional') && !is_null($request->id_profesional), function ($collection) use ($request) {
                    return $collection->where('created_by', $request->id_profesional);
                })
                ->when($request->has('bo_cancer') && !is_null($request->bo_cancer), function ($collection) use ($request) {
                    return $collection->where('bo_cancer', $request->bo_cancer);
                })
                ->when($request->has('id_estado') && !is_null($request->id_estado), function ($collection) use ($request) {
                    return $collection->whereIn('id_estado', [3,4]);
                })
                ->when(!$request->has('id_estado'), function ($collection) use ($request) {
                    return $collection->whereIn('id_estado', [0,1,2]);
                })
                ->when(Auth::user()->id_perfil == 3, function ($collection){
                    return $collection->whereIn('id_establecimiento', Auth::user()->establecimientos->pluck('id'));
                })
                ->when(Auth::user()->id_perfil == 4, function ($collection){
                    return $collection->where('id_estado', '!=', '0')->orderBy('id_estado', 'desc')->orderBy('bo_cancer', 'desc');
                })
                ->orderBy('id', 'desc')
                ->paginate(10);
        // dd(Auth::user()->establecimientos);
        $profesionales = User::whereIn('id_perfil', [1,3])->get();
        $establecimientos = Auth::user()->establecimientos;
        if(count($establecimientos) < 1){
            $establecimientos = Establecimiento::where("id_servicio", 10)->get();
        }
        return view('indexReferenciaOdontologica', compact('referenciasOdontologicas', 'establecimientos', 'profesionales'));
    }

    public function getDiagnostico(Request $request)
    {
        $diagnosticos = Cie10::where('bo_estado', 1)->whereRaw("CONCAT(cd_diagnostico,' ',tx_descripcion) LIKE ?", ['%'.$request->search.'%'])->get();
        $data['results'] = array();
        foreach($diagnosticos as $key => $diagnostico){
            $producto = array();
            $producto['id'] = $diagnostico->id;
            $producto['text'] = "{$diagnostico->cd_diagnostico} {$diagnostico->tx_descripcion}"; 
            array_push($data['results'], $producto);
        }
        $data['pagination'] = ["more" => false];
        return $data;
    }

    public function detalle($id_referencia_odontologica)
    {
        $referenciaOdontologica = ReferenciaOdontologica::with('mucosas', 'oseas')->where('id',$id_referencia_odontologica)->first();
        $paciente = Paciente::where('id', $referenciaOdontologica->id_paciente)->first();
        return view('modal-detalle', compact('referenciaOdontologica', 'paciente'));
    }

    public function consentimiento($id_referencia_odontologica)
    {
        $referenciaOdontologica = ReferenciaOdontologica::with('mucosas', 'oseas')->where('id',$id_referencia_odontologica)->first();
        $paciente = Paciente::where('id', $referenciaOdontologica->id_paciente)->first();
        return view('modal-consentimiento', compact('referenciaOdontologica', 'paciente'));
    }

    public function revocacion($id_referencia_odontologica)
    {
        $referenciaOdontologica = ReferenciaOdontologica::with('mucosas', 'oseas')->where('id',$id_referencia_odontologica)->first();
        $paciente = Paciente::where('id', $referenciaOdontologica->id_paciente)->first();
        return view('modal-revocacion', compact('referenciaOdontologica', 'paciente'));
    }

    public function seguimiento($id_referencia_odontologica)
    {
        $referenciaOdontologica = ReferenciaOdontologica::with('mucosas', 'oseas')->where('id',$id_referencia_odontologica)->first();
        $paciente = Paciente::where('id', $referenciaOdontologica->id_paciente)->first();
        return view('modal-seguimiento', compact('referenciaOdontologica', 'paciente'));
    }

    public function storeSeguimiento(Request $request)
    {
        $seguimiento = Seguimiento::create($request->all());
        return redirect()->back()->with('message', "Seguimiento Registrado");
    }

    public function deleteReferenciaOdontologica($id)
    {
        $referenciaOdontologica = ReferenciaOdontologica::find($id);
        if($referenciaOdontologica->delete()){
            return redirect('/referenciaOdontologica')->with('message', "La Referencia Odontologica a sido eliminado correctamente");
        }else{
            return redirect('/referenciaOdontologica')->with('error', "La Referencia Odontologica no a sido eliminado, intente nuevamente");
        }
    }

    public function pdfReferenciaOdontologica(Request $request){ //Agregar fecha al pie de pagina
        $referenciaOdontologica = ReferenciaOdontologica::with('mucosas', 'oseas')->where('id', request()->id)->first();
        $paciente = Paciente::where('id', $referenciaOdontologica->id_paciente)->first();
        $today = date("Y-m-d H:i:s");
        $today = Carbon::now()->format('d/m/Y');
        $pdf = \PDF::loadView('pdfReferenciaOdontologica', compact('paciente', 'referenciaOdontologica', 'today'));
        return $pdf->stream('referencia_odontologica.pdf');
    }

    public function pdfConsentimientoInformado(Request $request){
        $referenciaOdontologica = ReferenciaOdontologica::where('id', request()->id)->first();
        $today = date("Y-m-d H:i:s");
        $today = ucwords(\Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$today)->locale('es')->isoFormat('D \\d\\e MMMM \\d\\e YYYY'));
        $pdf = \PDF::loadView('pdfConsentimientoInformado', compact('paciente', 'referenciaOdontologica', 'today'));
        return $pdf->stream('consentimiento_informado.pdf');
    }

    public function pdfRevocacionConsentimientoInformado(Request $request){
        $referenciaOdontologica = ReferenciaOdontologica::where('id', request()->id)->first();
        $paciente = Paciente::where('id', $referenciaOdontologica->id_paciente)->first();
        $today = Carbon::now()->format('d/m/Y');
        $pdf = \PDF::loadView('pdfRevocacionConsentimientoInformado', compact('paciente', 'referenciaOdontologica'));
        return $pdf->stream('revocacion_consentimiento_informado.pdf');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $referenciaOdontologica = ReferenciaOdontologica::with('paciente', 'mucosas', 'oseas')->where('id', request()->id)->first();
        $previsiones = Prevision::where("bo_estado", 1)->get();
        $clasificacionesFonasa = ClasificacionFonasa::where("bo_estado", 1)->get();
        $establecimientos = Auth::user()->establecimientos;
        $comunas = Comuna::where('bo_estado', '1')->orderBy('tx_descripcion')->get();
        $mucosas = Mucosa::orderBy('id')->get();
        $oseas = Osea::orderBy('id')->get();
        $diagnosticos = Cie10::where('bo_estado', '1')->get();
        $unidades_tiempo = UnidadTiempo::orderBy('id')->get();
        return view('createReferenciaOdontologica', compact('previsiones', 'clasificacionesFonasa', 'establecimientos', 'comunas', 'mucosas', 'oseas', 'diagnosticos', 'referenciaOdontologica', 'unidades_tiempo'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $referenciaOdontologica = ReferenciaOdontologica::find(request()->id);

        if(isset(request()->id) && isset($referenciaOdontologica) && $referenciaOdontologica->id_estado != 0){
            return redirect('referenciaOdontologica')->with('error', "La Referencia Odontologica ya ha sido enviada, no es posible editarla");
        }

        //PACIENTE
        $id_clasificacion_fonasa = NULL;
        if(request()->id_tipo_identificacion_paciente == 1){
            $rutSinPuntos = str_replace(".","",request()->rut);
            $rut = explode("-",$rutSinPuntos);
        }else{
            $pasaporte = request()->rut;
        }
        if(is_numeric(request()->id_prevision)){
            $id_prevision = request()->id_prevision;
        }else{
            $id_prevision = 1;
            $fonasa = ClasificacionFonasa::where("cd_clasificacion_fonasa", request()->id_prevision)->first();
            $id_clasificacion_fonasa = $fonasa->id;
        }
        if(request()->id_tipo_identificacion_paciente == 1){
            $paciente = Paciente::updateOrCreate([
                'nr_run' => $rut[0]
            ], [
                'id_tipo_identificacion_paciente'=> request()->id_tipo_identificacion_paciente,
                'nr_run'=> $rut[0],
                'tx_digito_verificador'=> $rut[1],
                'fc_nacimiento'=> request()->fc_nacimiento,
                'id_sexo'=> request()->id_sexo,
                'tx_nombre'=> ucwords(request()->tx_nombre),
                'tx_apellido_paterno'=> ucwords(request()->tx_apellido_paterno),
                'tx_apellido_materno'=> ucwords(request()->tx_apellido_materno),
                'tx_direccion'=> request()->tx_direccion,
                'id_comuna'=> request()->id_comuna_paciente,
                'tx_telefono'=> request()->tx_telefono,
                'id_prevision'=> $id_prevision,
                'id_clasificacion_fonasa'=> $id_clasificacion_fonasa,
                'created_by'=> Auth::user()->id,
            ]);
        }else{
            $paciente = Paciente::updateOrCreate([
                'tx_pasaporte' => $pasaporte
            ], [
                'id_tipo_identificacion_paciente'=> request()->id_tipo_identificacion_paciente,
                'tx_pasaporte'=> $pasaporte,
                'fc_nacimiento'=> request()->fc_nacimiento,
                'id_sexo'=> request()->id_sexo,
                'tx_nombre'=> ucwords(request()->tx_nombre),
                'tx_apellido_paterno'=> ucwords(request()->tx_apellido_paterno),
                'tx_apellido_materno'=> ucwords(request()->tx_apellido_materno),
                'tx_direccion'=> request()->tx_direccion,
                'tx_telefono'=> request()->tx_telefono,
                'id_prevision'=> $id_prevision,
                'id_clasificacion_fonasa'=> $id_clasificacion_fonasa,
                'created_by'=> Auth::user()->id,
            ]);
        }

        //REFERENCIA ODONTOLOGICA
        $referenciaOdontologica = ReferenciaOdontologica::updateOrCreate([
            'id' => request()->id
        ], [
            'id_paciente'=> $paciente->id,
            'id_establecimiento'=> request()->id_establecimiento,
            'id_cie10'=> request()->id_cie10,
            'bo_cancer'=> request()->bo_cancer,
            'prioridad'=> request()->prioridad,
            'sospecha_diagnostica'=> request()->sospecha_diagnostica,
            'motivo_consulta'=> request()->motivo_consulta,
            'antecedente_morbido'=> request()->antecedente_morbido,
            'alergia'=> request()->alergia,
            'tamano_lesion'=> request()->tamano_lesion,
            'tm_evolucion'=> request()->tm_evolucion,
            'id_unidad_tiempo'=> request()->id_unidad_tiempo,
            'sintomatologia'=> request()->sintomatologia,
            'otro'=> request()->otro_lesion,
            'id_estado'=> request()->id_estado,
            'created_by'=> Auth::user()->id,
        ]);

        if(isset($request->consentimiento)){
            $consentimiento = $request->file('consentimiento');
            $nombre = $consentimiento->getClientOriginalName();
            $nombre = substr($nombre, -4, 4);
            $nombre = 'ci'.$referenciaOdontologica->id.$nombre;
            \Storage::disk('local')->put($nombre,  \File::get($consentimiento));
            $referenciaOdontologica->consentimiento = $nombre;
            $referenciaOdontologica->save();
        }

        if(isset($request->foto_1)){
            $foto_1 = $request->file('foto_1');
            $nombre = $foto_1->getClientOriginalName();
            $nombre = substr($nombre, -4, 4);
            $nombre = 'f1_'.$referenciaOdontologica->id.$nombre;
            \Storage::disk('local')->put($nombre, \File::get($foto_1));
            $referenciaOdontologica->foto_1 = $nombre;
            $referenciaOdontologica->save();
        }

        if(isset($request->foto_2)){
            $foto_2 = $request->file('foto_2');
            $nombre = $foto_2->getClientOriginalName();
            $nombre = substr($nombre, -4, 4);
            $nombre = 'f2_'.$referenciaOdontologica->id.$nombre;
            \Storage::disk('local')->put($nombre,  \File::get($foto_2));
            $referenciaOdontologica->foto_2 = $nombre;
            $referenciaOdontologica->save();
        }

        //HABITO
        $habito = Habito::updateOrCreate([
            'id_paciente'=> $paciente->id
        ],[
            'id_paciente'=> $paciente->id,
            'nr_cigarrillo'=> request()->nr_cigarrillo,
            'alcohol'=> request()->id_consumo_alcohol,
            'observacion'=> request()->observacion,
            'otro'=> request()->otro,
            'medicamento'=> request()->medicamento,
            'created_by'=> Auth::user()->id,
        ]);

        //MUCOSA
        if(isset(request()->mucosa)){
            $referenciaOdontologica->mucosas()->sync($request->mucosa);
        }else{
            $referenciaOdontologica->mucosas()->sync(null);
        }

        //OSEA
        if(isset(request()->osea)){
            $referenciaOdontologica->oseas()->sync($request->osea);
        }else{
            $referenciaOdontologica->oseas()->sync(null);
        }

        if($paciente->id_tipo_identificacion_paciente == 1){
            $rut = 'rut='.$rut[0].'-'.$rut[1];
        }else{
            $rut = 'pasaporte='.$paciente->tx_pasaporte;
        }
        if($referenciaOdontologica){
            if(request()->consentimiento_informado == 1){
                return redirect('/referenciaOdontologica/create?id='.$referenciaOdontologica->id.'&ci=1');
            }
            return redirect('/paciente?'.$rut)->with('message', "Se ha creado la Referencia Odontologica");
        }else{
            return redirect('referenciaOdontologica')->with('error', "No se ha creado la Referencia Odontologica");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ReferenciaOdontologica  $referenciaOdontologica
     * @return \Illuminate\Http\Response
     */
    public function show(ReferenciaOdontologica $referenciaOdontologica)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ReferenciaOdontologica  $referenciaOdontologica
     * @return \Illuminate\Http\Response
     */
    public function edit(ReferenciaOdontologica $referenciaOdontologica)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ReferenciaOdontologica  $referenciaOdontologica
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ReferenciaOdontologica $referenciaOdontologica)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ReferenciaOdontologica  $referenciaOdontologica
     * @return \Illuminate\Http\Response
     */
    public function destroy(ReferenciaOdontologica $referenciaOdontologica)
    {
        //
    }
}
