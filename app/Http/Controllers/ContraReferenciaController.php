<?php

namespace App\Http\Controllers;

use App\Paciente;
use Illuminate\Http\Request;
use Auth;
use App\ReferenciaOdontologica;
use App\ContraReferencia;
use App\CausalNumero;
use App\MotivoNoPertinente;
use Carbon\Carbon;

class ContraReferenciaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function pdfContraReferencia(Request $request){ //Agregar fecha al pie de pagina
        $referenciaOdontologica = ReferenciaOdontologica::with('contraReferencia', 'mucosas', 'oseas')->where('id', request()->id)->first();
        $paciente = Paciente::where('id', $referenciaOdontologica->id_paciente)->first();
        $today = Carbon::now()->format('d/m/Y');
        $pdf = \PDF::loadView('pdfContraReferencia', compact('paciente', 'referenciaOdontologica', 'today'));
        return $pdf->stream('contra_referencia.pdf');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $contraReferencia = ContraReferencia::where('id_referencia_odontologica', request()->id)->first();
        if(isset($contraReferencia) && in_array($contraReferencia->referenciaOdontologica->id_estado, [3, 4])){
            return redirect('referenciaOdontologica')->with('error', "La Contra Referencia ya ha sido creada");
        }
        $referenciaOdontologica = ReferenciaOdontologica::with('mucosas', 'oseas')->where('id', request()->id)->first();
        $paciente = Paciente::where('id', $referenciaOdontologica->id_paciente)->first();
        $causalesNumero = CausalNumero::get();
        $motivosNoPertinente = MotivoNoPertinente::get();
        return view('createContraReferencia', compact('paciente', 'referenciaOdontologica', 'contraReferencia', 'causalesNumero', 'motivosNoPertinente'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $contraReferencia = ContraReferencia::updateOrCreate([
            'id_referencia_odontologica' => request()->id_referencia_odontologica
        ], [
            'urgente'=> request()->urgente,
            'control_aps'=> request()->control_aps,
            'control_hsjd'=> request()->control_hsjd,
            'egreso_aps'=> request()->egreso_aps,
            'id_causal_numero'=> request()->causal_numero,
            'derivacion'=> request()->derivacion,
            'id_motivo_no_pertinente'=> request()->motivo_no_pertinente,
            'observacion'=> request()->observacion,
            'motivo_rechazo'=> request()->motivo_rechazo,
            'created_by'=> Auth::user()->id,
        ]);
        $referenciaOdontologica = ReferenciaOdontologica::where('id', $request->id_referencia_odontologica)->update(['id_estado' => $request->id_estado]);
        $referenciaOdontologica = ReferenciaOdontologica::where('id', $request->id_referencia_odontologica)->first();
        $paciente = Paciente::where('id', $referenciaOdontologica->id_paciente)->first();
        $rut = 'rut='.$paciente->nr_run.'-'.$paciente->tx_digito_verificador;
        if($contraReferencia){
            return redirect('/paciente?'.$rut)->with('message', "Se ha creado la Contra Referencia");
        }else{
            return redirect('referenciaOdontologica')->with('error', "No se ha creado la Contra Referencia");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Paciente  $paciente
     * @return \Illuminate\Http\Response
     */
    public function show(Paciente $paciente)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Paciente  $paciente
     * @return \Illuminate\Http\Response
     */
    public function edit(Paciente $paciente)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Paciente  $paciente
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Paciente $paciente)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Paciente  $paciente
     * @return \Illuminate\Http\Response
     */
    public function destroy(Paciente $paciente)
    {
        //
    }
}
