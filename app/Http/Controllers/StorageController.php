<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Paciente;
use App\ReferenciaOdontologica;

class StorageController extends Controller
{
    // public function index()
    // {
    //     return \View::make('new');
    // }

    public function save(Request $request)
    { 
        //obtenemos el campo file definido en el formulario
        $file = $request->file('file');

        //obtenemos el nombre del archivo
        $nombre = $file->getClientOriginalName();
        $nombre = substr($nombre, -4, 4);
        
        $referenciaOdontologica = ReferenciaOdontologica::find($request->id);
        $paciente = Paciente::find($referenciaOdontologica->id_paciente);
        if($paciente->id_tipo_identificacion_paciente == 1){
            $rut = 'rut='.$paciente->nr_run.'-'.$paciente->tx_digito_verificador;
        }else{
            $rut = 'pasaporte='.$paciente->tx_pasaporte;
        }
        if($request->consentimiento == 1){
            $nombre = 'ci'.$referenciaOdontologica->id.$nombre;
            //indicamos que queremos guardar un nuevo archivo en el disco local
            \Storage::disk('local')->put($nombre,  \File::get($file));
            $referenciaOdontologica->consentimiento = $nombre;
            $referenciaOdontologica->save();
            return redirect()->route('paciente', $rut)->with('message', "Se ha Subido el Consentimiento Informado");
        }else {
            $nombre = 'rci'.$referenciaOdontologica->id.$nombre;
            //indicamos que queremos guardar un nuevo archivo en el disco local
            \Storage::disk('local')->put($nombre,  \File::get($file));
            $referenciaOdontologica->revocacion = $nombre;
            $referenciaOdontologica->save();
            return redirect()->route('paciente', $rut)->with('message', "Se ha Subido la Revocacion del Consentimiento Informado");
        }
    }
}
