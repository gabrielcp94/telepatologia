<?php

namespace App\Http\Controllers;

use App\User;
use App\Perfil;
use Illuminate\Support\Facades\Mail;
use App\Mail\InformationReceived;
use Auth;
use App\Establecimiento;
use Illuminate\Http\Request;

class UsuarioController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $usuarios = User::latest('id')
                ->when($request->has('rut') && !is_null($request->rut), function ($collection) use ($request) {
                    return $collection->whereRaw("rut LIKE ?", ['%'.$request->rut.'%']);
                })
                ->when($request->has('nombre') && !is_null($request->nombre), function ($collection) use ($request) {
                    return $collection->whereRaw("CONCAT(`tx_nombre`, ' ', `tx_apellido_paterno`, ' ', `tx_apellido_materno`) LIKE ?", ['%'.$request->nombre.'%']);
                })
                ->paginate(10);

        return view('indexUsuario', compact('usuarios'));
    }

    public function sendMail()
    {
        $usuario = User::find(Auth::user()->id);
        $establecimientos = Establecimiento::where("id_servicio", 10)->get();
        $usuarios = User::get();
        return view('sendMail', compact('usuario', 'establecimientos', 'usuarios'));
    }

    public function storeMail(Request $request)
    {
        // dd($request->mensaje);
        // return view('information', compact('request'));
        Mail::to($request->receptor)->send(new InformationReceived($request));

        return redirect('/referenciaOdontologica')->with('message', "El correo ha sido enviado correctamente");
    }

    public function editUsuario($id)
    {
        $usuario = User::with('establecimientos')->find($id);
        $perfiles = Perfil::get();
        $establecimientos = Establecimiento::where("id_servicio", 10)->get();
        return view('editUsuario', compact('usuario', 'perfiles', 'establecimientos'));  
    }

    public function storeUsuario(Request $request)
    {
        $usuario = User::find($request->id);
        $usuario->tx_email = $request->tx_email;
        $usuario->id_perfil = $request->id_perfil;
        $usuario->save();
        $usuario->establecimientos()->sync($request->establecimientos);

        return redirect('/usuario')->with('message', "El usuario a sido editado correctamente");
    }

    public function editPassword()
    {
        $usuario = User::find(Auth::user()->id);
        return view('editPassword', compact('usuario'));
    }

    public function storePassword(Request $request)
    {
        $usuario = User::find(Auth::user()->id);
        if(password_verify($request->password_actual, $usuario->password)){
            if($request->password_nueva == $request->password_nueva_2){
                $usuario->password = $request->password_nueva;
                $usuario->save();
                return redirect('referenciaOdontologica')->with('message', "La contraseña a sido cambiada exitosamente");
            }else{
                return redirect()->back()->with('error', "Las nuevas contraseñas no coinciden");
            }
        }else{
            return redirect()->back()->with('error', "La contraseña actual no es correcta");
        }
    }

    public function resetUsuario($id) // Reiniciar Contraseña
    {
        $usuario = User::find($id);
        $password = substr($usuario->rut, 0, 4);
        $usuario->password = $password;
        $usuario->save();

        return redirect('/usuario')->with('message', "La contraseña del usuario a sido reiniciada");
    }

    public function deleteUsuario($id)
    {
        $usuario = User::find($id);
        if($usuario->delete()){
            return redirect('/usuario')->with('message', "El usuario a sido eliminado correctamente");
        }else{
            return redirect('/usuario')->with('error', "El usuario no a sido eliminado, intente nuevamente");
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $perfiles = Perfil::get();
        $establecimientos = Establecimiento::where("id_servicio", 10)->get();
        return view('createUsuario', compact('perfiles', 'establecimientos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rut = str_replace(".","",$request->rut);
        $validar = User::where('rut', $rut)->first();
        if($validar){
            return redirect()->route('home')->with('error', "El usuario ".$validar->nombre_completo." ya ha sido creado anteriormente");
        }
        $password = substr($rut, 0, 4);
        $user = User::create([
            'rut'=> $rut,
            'tx_nombre' => $request->tx_nombre,
            'tx_apellido_paterno' => $request->tx_apellido_paterno,
            'tx_apellido_materno' => $request->tx_apellido_materno,
            'tx_email' => $request->tx_email,
            'id_perfil' => $request->id_perfil,
            'password' => $password,
        ]);

        if(isset($request->establecimientos)){
            $user->establecimientos()->sync($request->establecimientos);
        }

        if($user){
            return redirect('/usuario')->with('message', "Se ha creado el usuario ".$user->nombre_completo." exitosamente");
        }else{
            return redirect('/usuario')->with('error', "Error. El usuario no ha podido ser creado");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        // 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }
}
