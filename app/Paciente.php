<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Paciente extends Model
{
    protected $connection = 'mysql';
    protected $table = 'paciente';

    use SoftDeletes;

    public $guarded = [];
    protected $appends = ['nombre_completo', 'rut', 'edad'];

    public function setFcNacimientoAttribute($value) 
	  {
	    $this->attributes['fc_nacimiento'] = date("Y-m-d", strtotime(str_replace("/",".", $value)));
    }

    public function getNombreCompletoAttribute()
    {
        return "{$this->tx_nombre} {$this->tx_apellido_paterno} {$this->tx_apellido_materno}";
    }

    public function getRutAttribute()
    {
        if ($this->nr_run == 0 || $this->nr_run == null): return "Sin Información"; endif;
        $nr_run = $this->formatCLP($this->nr_run);
        return "{$nr_run}-{$this->tx_digito_verificador}";
    }

    function formatCLP($n)
    {
        return is_int($n) ? number_format($n, 0, ',', '.') : null;
    }

    public function getEdadAnosAttribute(){
      if ($this->fc_nacimiento != null) {
          $f1 = Carbon::createFromFormat('Y-m-d', $this->fc_nacimiento);
          $f2 = Carbon::now();
          return $f2->diff($f1)->y;
      }
      return false;
  }
  
  public function getEdadAttribute(){
      if ($this->fc_nacimiento != null) {
          $f1 = Carbon::createFromFormat('Y-m-d', $this->fc_nacimiento);
          $f2 = Carbon::now();
          $anos = $f2->diff($f1)->format('%y años');
          // solo si es menor de 3 años mostrar años con meses y días
          if ($f2->diff($f1)->y < 3) {
              $dias = $f2->diff($f1)->format('%d días');
              $meses = $f2->diff($f1)->format('%m meses');
              $anos = $f2->diff($f1)->format('%y años');

              if($f2->diff($f1)->format('%d') == '1')
                  $dias = $f2->diff($f1)->format('%d día');
              if($f2->diff($f1)->format('%m') == '1')
                  $meses = $f2->diff($f1)->format('%m mes');
              if($f2->diff($f1)->format('%y') == '1')
                  $anos = $f2->diff($f1)->format('%y año');

              if ($f2->diff($f1)->y == 0) {
                  if ($f2->diff($f1)->format('%m') == '0'){
                      return $dias;
                  } 
                  return "{$meses} y {$dias}";
              }
              
              return "{$anos} con {$meses} y {$dias}";
          }
          return $anos;
      }
      return false;
  }

    public function prevision()
    {
		return $this->belongsTo('App\Prevision', 'id_prevision')->withDefault(["tx_descripcion" => "Sin Información"]);
    }

    public function clasificacionFonasa()
    {
		return $this->belongsTo('App\ClasificacionFonasa', 'id_clasificacion_fonasa')->withDefault(["tx_descripcion" => ""]);
    }
    
    public function sexo()
    {
		return $this->belongsTo('App\Sexo', 'id_sexo')->withDefault(["tx_descripcion" => "Sin Información"]);
    }

    public function comuna()
    {
		return $this->belongsTo('App\Comuna', 'id_comuna', 'cd_comuna')->withDefault(["tx_descripcion" => "Sin Información"]);
    }

    public function pais()
    {
		return $this->belongsTo('App\Pais', 'id_pais')->withDefault(["tx_descripcion" => "Sin Información"]);
    }

    public function referenciasOdontologicas()
    {
		return $this->hasMany('App\ReferenciaOdontologica', 'id_paciente');
    }

    public function habito()
    {
		return $this->hasOne('App\Habito', 'id_paciente');
    }
}