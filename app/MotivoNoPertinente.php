<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MotivoNoPertinente extends Model
{
    protected $connection = 'mysql';
    protected $table = 'motivo_no_pertinente';
}