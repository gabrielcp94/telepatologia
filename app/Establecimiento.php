<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Establecimiento extends Model
{
    protected $connection = 'mysql';
    protected $table = 'gen_establecimiento';

    public function comuna()
    {
		return $this->belongsTo('App\Comuna', 'id_comuna');
    }
}