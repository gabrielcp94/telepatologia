<?php

namespace App;

use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable;

    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'password', 'tx_nombre', 'tx_apellido_paterno', 'tx_apellido_materno', 'tx_email', 'rut', 'id_perfil',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $appends = ['nombre_completo'];

    public function getNombreCompletoAttribute()
    {
        return "{$this->tx_nombre} {$this->tx_apellido_paterno} {$this->tx_apellido_materno}";
    }

    public function setPasswordAttribute($value){
        $this->attributes['password'] = bcrypt(intval($value));
    }

    public function perfil()
    {
		return $this->belongsTo('App\Perfil', 'id_perfil')->withDefault(["nombre" => "Sin Información"]);
    }

    // public function establecimiento()
    // {
	// 	return $this->hasMany('App\UserEstablecimiento', 'id_user');
    // }

    public static function boot()
    {
        parent::boot();
        
        static::creating(function($user)
		{
			$user->created_by = Auth::id();
		});
        
        static::created(function($user)
		{
			$user->created_by = Auth::id();
        });

        static::updating(function($user)
		{
            $user->updated_by = Auth::id();
        });

        static::updated(function($user)
		{
            $user->updated_by = Auth::id();
        });
        
        static::deleting(function($user) {
			$user->establecimientos()->sync([]);
		});
        
        static::deleted(function($user) {
			$user->establecimientos()->sync([]);
		});
	}

    public function establecimientos()
    {
        return $this->belongsToMany('App\Establecimiento', 'users_establecimiento', 'id_user', 'id_establecimiento');
    }
}
