<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/referenciaOdontologica');
});

Auth::routes();

Route::resource('/paciente', 'PacienteController');
Route::get('/getDatosRut', 'PacienteController@getDatosRut');
Route::get('/getComunaEstablecimiento', 'PacienteController@getComunaEstablecimiento');

Route::resource('/referenciaOdontologica', 'ReferenciaOdontologicaController');
Route::get('/getDiagnostico', 'ReferenciaOdontologicaController@getDiagnostico');
Route::get('/referenciaOdontologicaDetalle/{id}', 'ReferenciaOdontologicaController@detalle');
Route::get('/referenciaOdontologicaSeguimiento/{id}', 'ReferenciaOdontologicaController@seguimiento');
Route::post('/storeSeguimiento', 'ReferenciaOdontologicaController@storeSeguimiento');
Route::get('/referenciaOdontologicaConsentimiento/{id}', 'ReferenciaOdontologicaController@consentimiento');
Route::get('/referenciaOdontologicaRevocacion/{id}', 'ReferenciaOdontologicaController@revocacion');
Route::get('/deleteReferenciaOdontologica/{id}', 'ReferenciaOdontologicaController@deleteReferenciaOdontologica');
Route::name('print')->get('/pdfReferenciaOdontologica', 'ReferenciaOdontologicaController@pdfReferenciaOdontologica');
Route::name('print')->get('/pdfConsentimientoInformado', 'ReferenciaOdontologicaController@pdfConsentimientoInformado');
Route::name('print')->get('/pdfRevocacionConsentimientoInformado', 'ReferenciaOdontologicaController@pdfRevocacionConsentimientoInformado');

Route::resource('/contraReferencia', 'ContraReferenciaController');
Route::name('print')->get('/pdfContraReferencia', 'ContraReferenciaController@pdfContraReferencia');

Route::resource('/usuario', 'UsuarioController');
Route::get('/sendMail', 'UsuarioController@sendMail')->name('sendMail');
Route::post('/storeMail', 'UsuarioController@storeMail');
Route::get('/editUsuario/{id}', 'UsuarioController@editUsuario');
Route::post('/storeUsuario', 'UsuarioController@storeUsuario');
Route::get('/editPassword', 'UsuarioController@editPassword')->name('editPassword');
Route::post('/storePassword', 'UsuarioController@storePassword');
Route::get('/resetUsuario/{id}', 'UsuarioController@resetUsuario');
Route::get('/deleteUsuario/{id}', 'UsuarioController@deleteUsuario');

Route::get('formulario', 'StorageController@index');
Route::post('storage/create', 'StorageController@save');
Route::get('storage/{archivo}', function ($archivo) {
     $public_path = public_path();
     $url = $public_path.'/storage/'.$archivo;
     //verificamos si el archivo existe y lo retornamos
     if (Storage::exists($archivo))
     {
       return response()->download($url);
     }
     //si no se encuentra lanzamos un error 404.
     abort(404);
});