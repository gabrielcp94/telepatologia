<div class="modal fade" id="modal-detalle" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Referencia Odontologica ({{$referenciaOdontologica->id}})</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            {{-- <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Referencia Odontologica ({{$itemReferenciaOdontologica->id}})</h4>
            </div> --}}
            <div class="modal-body">
                <div class="row invoice-info">
                    <div class="col-sm-6 invoice-col">
                        <strong>Nombre: </strong>{{$paciente->nombre_completo}}<br>
                        <strong>Rut: </strong>{{$paciente->rut}}<br>
                        <strong>Fecha de Nacimiento: </strong>
                        @if($paciente->fc_nacimiento != '0000-00-00')
                            {{date('d/m/Y', strtotime(str_replace("/",".",$paciente->fc_nacimiento)))}}
                        @else 
                            Sin Información
                        @endif<br>
                        <strong>Previsión: </strong>{{$paciente->prevision->tx_descripcion}} {{$paciente->clasificacionFonasa->tx_descripcion}}
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-6 invoice-col">
                        <strong>Sexo: </strong>{{$paciente->sexo->tx_descripcion}}<br>
                        <strong>Edad: </strong>{{$paciente->edad}}<br>
                        <strong>Dirección: </strong>{{$paciente->tx_direccion}} ({{$paciente->comuna->tx_descripcion}})<br>
                        <strong>Telefono: </strong>
                        @if(isset($paciente->tx_telefono))
                            {{$paciente->tx_telefono}}
                        @else
                            Sin Información
                        @endif
                    </div>
                    <!-- /.col -->
                </div>
                <br>
                <div class="row invoice-info">
                    <div class="col-sm-6 invoice-col">
                        <strong>Fumador: </strong>
                        @if($paciente->habito->nr_cigarrillo > 0)
                            Si
                        @else
                            No
                        @endif
                        <br>
                        <strong>Consumo de Alcohol: </strong>
                        @if($paciente->habito->alcohol == 1)
                            Si
                        @else
                            No
                        @endif
                        <br>
                        <strong>Otros: </strong>{{$paciente->habito->otro}}
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-6 invoice-col">
                        <strong>Número de Cigarillos Diarios: </strong>{{$paciente->habito->nr_cigarrillo}}<br>
                        <strong>Observacion: </strong>{{$paciente->habito->observacion}}<br>
                        <strong>Medicamentos: </strong>{{$paciente->habito->medicamento}}
                    </div>
                    <!-- /.col -->
                </div>
                <br>
                <div class="row invoice-info">
                    <div class="col-sm-6 invoice-col">
                        <strong>Profesional: </strong>{{$referenciaOdontologica->profesional->nombre_completo}}<br>
                        <strong>Rut: </strong>{{$referenciaOdontologica->profesional->rut}}<br>
                        <strong>Fecha Solicitud: </strong>{{date('d/m/Y', strtotime(str_replace("/",".",$referenciaOdontologica->created_at)))}}
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-6 invoice-col">
                        <strong>Establecimiento: </strong>{{$referenciaOdontologica->establecimiento->tx_descripcion}}<br>
                        <strong>Comuna: </strong>{{$referenciaOdontologica->establecimiento->comuna->tx_descripcion}}
                    </div>
                    <!-- /.col -->
                </div>
                @if(isset($referenciaOdontologica->contraReferencia->motivo_rechazo))
                    <br>
                    <div class="row invoice-info">
                        <div class="col-sm-12 invoice-col">
                            <strong>Motivo de Rechazo: {{$referenciaOdontologica->contraReferencia->motivo_rechazo}}</strong>
                        </div>
                    </div>
                @endif
                <br>
                <table class="table table-bordered">
                    <tbody>
                        <tr>
                            <td><strong>Estado:</strong></td>
                            <td><small class="badge badge-{{$referenciaOdontologica->estado->clase}}">{{$referenciaOdontologica->estado->nombre}}</small>
                                @if($referenciaOdontologica->bo_cancer == 1)
                                    <small class="badge badge-danger">S. Cancer</small>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td><strong>Diagnostico Cie10:</strong></td>
                            <td>{{$referenciaOdontologica->cie10->nombre_completo}}</td>
                        </tr>
                        <tr>
                            <td><strong>Sospecha Diagnostica:</strong></td>
                            <td>{{$referenciaOdontologica->sospecha_diagnostica}}</td>
                        </tr>
                        <tr>
                            <td><strong>Motivo de Consulta:</strong></td>
                            <td>{{$referenciaOdontologica->motivo_consulta}}</td>
                        </tr>
                        <tr>
                            <td><strong>Antecedentes Mórbicos:</strong></td>
                            <td>{{$referenciaOdontologica->antecedente_morbido}}</td>
                        </tr>
                        <tr>
                            <td><strong>Alergias:</strong></td>
                            <td>{{$referenciaOdontologica->alergia}}</td>
                        </tr>
                        <tr>
                            <td><strong>Tamaño de Lesión:</strong></td>
                            <td>{{$referenciaOdontologica->tamano_lesion}} (mm)</td>
                        </tr>
                        <tr>
                            <td><strong>Tiempo de Evolución:</strong></td>
                            <td>{{$referenciaOdontologica->tm_evolucion}} {{$referenciaOdontologica->unidad_tiempo->nombre}}</td>
                        </tr>
                        <tr>
                            <td><strong>Sintomatología:</strong></td>
                            <td>{{$referenciaOdontologica->sintomatologia}}</td>
                        </tr>
                        <tr>
                            <td><strong>Otros (Forma, Color, Consistencia, Etc):</strong></td>
                            <td>{{$referenciaOdontologica->otro}}</td>
                        </tr>
                        <tr>
                            <td><strong>Lesión Mucosa:</strong></td>
                            <td>{{$referenciaOdontologica->mucosas->pluck('id')->implode(', ')}}</td>
                        </tr>
                        <tr>
                            <td><strong>Lesión Ósea:</strong></td>
                            <td>{{$referenciaOdontologica->oseas->pluck('id')->implode(', ')}}</td>
                        </tr>
                        @if(isset($referenciaOdontologica->foto_1))
                            <tr>
                                <td><strong>Fotografía 1:</strong></td>
                                <td><a href="/storage/{{$referenciaOdontologica->foto_1}}" target="_blank" class="btn btn-success btn-xs"><i class="fa fa-eye" style="color:white"></i></a></td>
                            </tr>
                        @endif
                        @if(isset($referenciaOdontologica->foto_2))
                            <tr>
                                <td><strong>Fotografía 2:</strong></td>
                                <td><a href="/storage/{{$referenciaOdontologica->foto_2}}" target="_blank" class="btn btn-success btn-xs"><i class="fa fa-eye" style="color:white"></i></a></td>
                            </tr>
                        @endif
                    </tbody>
                </table>
                @if(!$referenciaOdontologica->seguimientos->isEmpty())
                    <br>
                    <div class="col-sm-6 invoice-col">
                        <strong>Seguimiento:</strong><br>
                    </div>
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <td><strong>Fecha:</strong></td>
                                <td><strong>Comentario</strong></td>
                            </tr>
                            @foreach ($referenciaOdontologica->seguimientos->sortByDesc('fc') as $seguimiento)
                                <tr>
                                    <td>{{date('d/m/Y', strtotime(str_replace("/",".",$seguimiento->fc)))}}</td>
                                    <td>{{$seguimiento->comentario}}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                @endif
            </div>
        </div>
              <!-- /.modal-content -->
    </div>
            <!-- /.modal-dialog -->
</div>