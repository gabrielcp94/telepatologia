@extends('adminlte::page')

@section('title', 'Ingresar Contra Referencia')

@section('content')
		{{-- <form class="form-horizontal" id="form" method="POST">
			{{ csrf_field() }} --}}
			

	<div class="card card-info">
		<div class="card-header">
		<h3 class="card-title">Registrar Contra Referencia</h3>
		</div>
		<!-- /.card-header -->
		<!-- form start -->
		<form role="form" class="form-horizontal" id="form" method="POST" action="{{action('ContraReferenciaController@store')}}">
			{{ csrf_field() }}
			<input type="hidden" id="id_referencia_odontologica" name="id_referencia_odontologica" value="{{$referenciaOdontologica->id}}">	
			<input type="hidden" id="created_by" name="created_by" value={{Auth::user()->id}} />	
			<input type="hidden" id="id_estado" name="id_estado"/>
			<div class="card-body">
				<h4>Datos del Paciente</h4>
				<hr>
				<div class="modal-body">
					<div class="row invoice-info">
						<div class="col-sm-6 invoice-col">
							<strong>Nombre: </strong>{{$paciente->nombre_completo}}<br>
							<strong>Rut: </strong>{{$paciente->rut}}<br>
							<strong>Fecha de Nacimiento: </strong>
							@if($paciente->fc_nacimiento != '0000-00-00')
								{{date('d/m/Y', strtotime(str_replace("/",".",$paciente->fc_nacimiento)))}}
							@else 
								Sin Información
							@endif<br>
							<strong>Previsión: </strong>{{$paciente->prevision->tx_descripcion}} {{$paciente->clasificacionFonasa->tx_descripcion}}
						</div>
						<!-- /.col -->
						<div class="col-sm-6 invoice-col">
							<strong>Sexo: </strong>{{$paciente->sexo->tx_descripcion}}<br>
							<strong>Edad: </strong>{{$paciente->edad}}<br>
							<strong>Dirección: </strong>{{$paciente->tx_direccion}} ({{$paciente->comuna->tx_descripcion}})<br>
							<strong>Telefono: </strong>
							@if(isset($paciente->tx_telefono))
								{{$paciente->tx_telefono}}
							@else
								Sin Información
							@endif
						</div>
						<!-- /.col -->
					</div>
					<br>
					<div class="row invoice-info">
						<div class="col-sm-6 invoice-col">
							<strong>Fumador: </strong>
							@if($paciente->habito->nr_cigarrillo > 0)
								Si
							@else
								No
							@endif
							<br>
							<strong>Consumo de Alcohol: </strong>
							@if($paciente->habito->alcohol == 1)
								Si
							@else
								No
							@endif
							<br>
							<strong>Otros: </strong>{{$paciente->habito->otro}}
						</div>
						<!-- /.col -->
						<div class="col-sm-6 invoice-col">
							<strong>Número de Cigarillos Diarios: </strong>{{$paciente->habito->nr_cigarrillo}}<br>
							<strong>Observacion: </strong>{{$paciente->habito->observacion}}<br>
							<strong>Medicamentos: </strong>{{$paciente->habito->medicamento}}
						</div>
						<!-- /.col -->
					</div>
					<br>
					<div class="row invoice-info">
						<div class="col-sm-6 invoice-col">
							<strong>Profesional: </strong>{{$referenciaOdontologica->profesional->nombre_completo}}<br>
							<strong>Rut: </strong>{{$referenciaOdontologica->profesional->rut}}<br>
							<strong>Fecha Solicitud: </strong>{{date('d/m/Y', strtotime(str_replace("/",".",$referenciaOdontologica->created_at)))}}
						</div>
						<!-- /.col -->
						<div class="col-sm-6 invoice-col">
							<strong>Establecimiento: </strong>{{$referenciaOdontologica->establecimiento->tx_descripcion}}<br>
							<strong>Comuna: </strong>{{$referenciaOdontologica->establecimiento->comuna->tx_descripcion}}
						</div>
						<!-- /.col -->
					</div>
					<br>
					<table class="table table-bordered">
						<tbody>
							<tr>
								<td><strong>Estado:</strong></td>
								<td><small class="badge badge-{{$referenciaOdontologica->estado->clase}}">{{$referenciaOdontologica->estado->nombre}}</small>
									@if($referenciaOdontologica->bo_cancer == 1)
										<small class="badge badge-danger">S. Cancer</small>
									@endif
								</td>
							</tr>
							<tr>
								<td><strong>Diagnostico Cie10:</strong></td>
								<td>{{$referenciaOdontologica->cie10->nombre_completo}}</td>
							</tr>
							<tr>
								<td><strong>Sospecha Diagnostica:</strong></td>
								<td>{{$referenciaOdontologica->sospecha_diagnostica}}</td>
							</tr>
							<tr>
								<td><strong>Motivo de Consulta:</strong></td>
								<td>{{$referenciaOdontologica->motivo_consulta}}</td>
							</tr>
							<tr>
								<td><strong>Antecedentes Mórbicos:</strong></td>
								<td>{{$referenciaOdontologica->antecedente_morbido}}</td>
							</tr>
							<tr>
								<td><strong>Alergias:</strong></td>
								<td>{{$referenciaOdontologica->alergia}}</td>
							</tr>
							<tr>
								<td><strong>Tamaño de Lesión:</strong></td>
								<td>{{$referenciaOdontologica->tamano_lesion}} (mm)</td>
							</tr>
							<tr>
								<td><strong>Tiempo de Evolución:</strong></td>
								<td>{{$referenciaOdontologica->tm_evolucion}}</td>
							</tr>
							<tr>
								<td><strong>Sintomatología:</strong></td>
								<td>{{$referenciaOdontologica->sintomatologia}}</td>
							</tr>
							<tr>
								<td><strong>Otros (Forma, Color, Consistencia, Etc):</strong></td>
								<td>{{$referenciaOdontologica->otro}}</td>
							</tr>
							<tr>
								<td style="width: 200px !important"><strong>Lesión Mucosa:</strong><br>
									<img src="{{url('/images/mucosa.jpg')}}">
								</td>
								<td>{{$referenciaOdontologica->mucosas->pluck('id')->implode(', ')}}</td>
							</tr>
							<tr>
								<td><strong>Lesión Ósea:</strong><br>
									<img src="{{url('/images/osea.png')}}">
								</td>
								<td>{{$referenciaOdontologica->oseas->pluck('id')->implode(', ')}}</td>
							</tr>
							@if(isset($referenciaOdontologica->foto_1))
								<tr>
									<td><strong>Fotografía 1:</strong></td>
									<td><a href="/storage/{{$referenciaOdontologica->foto_1}}" target="_blank" class="btn btn-success btn-xs"><i class="fa fa-eye" style="color:white"></i></a></td>
								</tr>
							@endif
							@if(isset($referenciaOdontologica->foto_2))
								<tr>
									<td><strong>Fotografía 2:</strong></td>
									<td><a href="/storage/{{$referenciaOdontologica->foto_2}}" target="_blank" class="btn btn-success btn-xs"><i class="fa fa-eye" style="color:white"></i></a></td>
								</tr>
							@endif
						</tbody>
					</table>
				</div>
				<br>
				<h4>Procedimiento a Seguir</h4>
				<hr>
				<div class="form-group">
					<div class="row">
						<div class="col-sm-4">
							<label for="derivacion">Derivación No Pertinente o a Otra Especialidad<span style="color:#FF0000";>*</span></label>
							<div class="row">
								<div class="col-sm-4">
									<label>Pertinente</label>
								</div>
								<div class="col-sm-1">
									<input class="form-check-input" type="radio" id="derivacion" name="derivacion" value="1" onchange="pertinente($(this).val());" {{isset($contraReferencia->derivacion) && $contraReferencia->derivacion == 1 ? "checked" : ""}}>
								</div>
								<div class="col-sm-5">
									<label>No Pertinente</label>
								</div>
								<div class="col-sm-1">
									<input class="form-check-input" type="radio" id="derivacion" name="derivacion" value="0" onchange="pertinente($(this).val());" {{isset($contraReferencia->derivacion) && $contraReferencia->derivacion == 0 ? "checked" : ""}}>
								</div>
							</div>
						</div>
						<div class="col-sm-4">
							<label for="derivacion">Motivo No Pertinente</label>
							<select class="form-control"  id="motivo_no_pertinente" name="motivo_no_pertinente" {{isset($contraReferencia->derivacion) && $contraReferencia->derivacion == 0 ? "" : "disabled"}}>
								<option value="">Seleccione Motivo</option>
								@foreach ($motivosNoPertinente as $motivoNoPertinente)
									<option value={{$motivoNoPertinente->id}} {{isset($contraReferencia->motivo_no_pertinente) && $contraReferencia->motivo_no_pertinente == $motivoNoPertinente->id ? "selected" : ""}}>{{$motivoNoPertinente->nombre}}</option>								
								@endforeach
							</select>
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-sm-4">
							<label for="urgente">Prioridad Para la Especialidad<span style="color:#FF0000";>*</span></label>
							<div class="row">
								<div class="col-sm-3">
									<label>Urgente</label>
								</div>
								<div class="col-sm-1">
									<input class="form-check-input" type="radio" id="urgente" name="urgente" value="1" {{isset($contraReferencia->urgente) && $contraReferencia->urgente == 1 ? "checked" : ""}}>
								</div>
								<div class="col-sm-4">
									<label>No Urgente</label>
								</div>
								<div class="col-sm-1">
									<input class="form-check-input" type="radio" id="urgente" name="urgente" value="0" {{isset($contraReferencia->urgente) && $contraReferencia->urgente == 0 ? "checked" : ""}}>
								</div>
							</div>
						</div>
						<div class="col-sm-4">
							<label for="control_aps">Control en APS por Odontólogo Tratante</label>
							<div class="row">
								<div class="col-sm-2">
									<label>Sí</label>
								</div>
								<div class="col-sm-1">
									<input class="form-check-input" type="radio" id="control_aps" name="control_aps" value="1" {{isset($contraReferencia->control_aps) && $contraReferencia->control_aps == 1 ? "checked" : ""}}>
								</div>
								<div class="col-sm-2">
									<label>No</label>
								</div>
								<div class="col-sm-1">
									<input class="form-check-input" type="radio" id="control_aps" name="control_aps" value="0" {{isset($contraReferencia->control_aps) && $contraReferencia->control_aps == 0 ? "checked" : ""}}>
								</div>
							</div>
						</div>
						<div class="col-sm-4">
							<label for="control_hsjd">Control en HSJD por Especialista (Patología Oral)</label>
							<div class="row">
								<div class="col-sm-2">
									<label>Sí</label>
								</div>
								<div class="col-sm-1">
									<input class="form-check-input" type="radio" id="control_hsjd" name="control_hsjd" value="1" {{isset($contraReferencia->control_hsjd) && $contraReferencia->control_hsjd == 1 ? "checked" : ""}}>
								</div>
								<div class="col-sm-2">
									<label>No</label>
								</div>
								<div class="col-sm-1">
									<input class="form-check-input" type="radio" id="control_hsjd" name="control_hsjd" value="0" {{isset($contraReferencia->control_hsjd) && $contraReferencia->control_hsjd == 0 ? "checked" : ""}}>
								</div>
							</div>
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-sm-4">
							<label for="egreso_aps">Egreso en APS<span style="color:#FF0000";>*</span></label>
							<div class="row">
								<div class="col-sm-2">
									<label>Sí</label>
								</div>
								<div class="col-sm-1">
									<input class="form-check-input" type="radio" id="egreso_aps" name="egreso_aps" value="1" {{isset($contraReferencia->egreso_aps) && $contraReferencia->egreso_aps == 1 ? "checked" : ""}}>
								</div>
								<div class="col-sm-2">
									<label>No</label>
								</div>
								<div class="col-sm-1">
									<input class="form-check-input" type="radio" id="egreso_aps" name="egreso_aps" value="0" {{isset($contraReferencia->egreso_aps) && $contraReferencia->egreso_aps == 0 ? "checked" : ""}}>
								</div>
							</div>
						</div>
						<div class="col-sm-4">
							<label for="causal_numero">Causal Número<span style="color:#FF0000";>*</span></label>
							<select class="form-control" id="causal_numero" name="causal_numero" required>
								<option value="">Seleccione Motivo</option>
								@foreach ($causalesNumero as $causalNumero)
									<option value={{$causalNumero->id}} {{isset($contraReferencia->causal_numero) && $contraReferencia->causal_numero == $causalNumero->id ? "selected" : ""}}>{{$causalNumero->nombre}}</option>								
								@endforeach
							</select>
						</div>
					</div>
					<br>
					<div class="form-group">
						<div class="row">
							<div class="col-sm-12">
								<label for="observacion">Indicaciones u Observaciones<span style="color:#FF0000";>*</span></label>
								<textarea class="form-control noresize" id="observacion" name="observacion">{{isset($contraReferencia->observacion) ? $contraReferencia->observacion : ""}}</textarea>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="row">
							<div class="col-sm-12">
								<label for="motivo_rechazo">Motivo de Rechazo (solo en caso de rechazar)</label>
								<textarea class="form-control noresize" id="motivo_rechazo" name="motivo_rechazo">{{isset($contraReferencia->motivo_rechazo) ? $contraReferencia->motivo_rechazo : ""}}</textarea>
							</div>
						</div>
					</div>
				</div>
			</div>
		  	<div class="card-footer text-right">
				<button type="submit" class="btn btn-warning" onclick="guardar(2)">Guardar Borrador</button>
				{{-- <button type="submit" class="btn btn-danger" onclick="guardar(4)">Rechazar</button> --}}
				<button type="submit" class="btn btn-success" onclick="guardar(3)">Resolver</button>
		  	</div>
		</form>
	  </div>
@stop
@section('js')
<script>
	function guardar(id_estado){
		$("#id_estado").val(id_estado);
		if(id_estado == 2){
			$("#urgente").css('display','').attr('required', false);
			$("#control_aps").css('display','').attr('required', false);
			$("#control_hsjd").css('display','').attr('required', false);
			$("#egreso_aps").css('display','').attr('required', false);
			$("#causal_numero").css('display','').attr('required', false);
			$("#derivacion").css('display','').attr('required', false);
			$("#observacion").css('display','').attr('required', false);
			$("#motivo_rechazo").css('display','').attr('required', false);
		}else if(id_estado == 4){
			$("#urgente").css('display','').attr('required', false);
			$("#control_aps").css('display','').attr('required', false);
			$("#control_hsjd").css('display','').attr('required', false);
			$("#egreso_aps").css('display','').attr('required', false);
			$("#causal_numero").css('display','').attr('required', false);
			$("#derivacion").css('display','').attr('required', false);
			$("#observacion").css('display','').attr('required', false);
			$("#motivo_rechazo").css('display','').attr('required', true);
		}else{
			$("#urgente").css('display','').attr('required', true);
			$("#control_aps").css('display','').attr('required', true);
			$("#control_hsjd").css('display','').attr('required', true);
			$("#egreso_aps").css('display','').attr('required', true);
			$("#causal_numero").css('display','').attr('required', true);
			$("#derivacion").css('display','').attr('required', true);
			$("#observacion").css('display','').attr('required', true);
			$("#motivo_rechazo").css('display','').attr('required', false);
			$("#motivo_rechazo").val(null);
		}
	}

	function pertinente(pertinente){
		if(pertinente == 0){
			$("#motivo_no_pertinente").css('display','').attr('disabled', false);
			$("#motivo_no_pertinente").css('display','').attr('required', true);
		}else{
			$("#motivo_no_pertinente").css('display','').attr('disabled', true);
		}
	}
</script>
@endsection

