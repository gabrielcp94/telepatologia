<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
    <title>Mensaje Telepatologia</title>
</head>
<body>
    <p>{{($request->mensaje)}}</p>
    <br>
    <br>
    <br>
    <br>
    <p>Mensaje Enviado Por: {{$request->emisor}} ({{$request->email}}).</p>
    <p>Favor no responder este correo, si tienes dudas comunicarte directamente al correo del emisor del mensaje.</p>
</body>
</html>