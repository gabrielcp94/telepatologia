@extends('adminlte::page')

@section('title', 'Cambiar Contraseña')

@section('content_header')
    @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @endif
    @if(session()->has('error'))
        <div class="alert alert-danger">
            <i class="fa fa-exclamation-triangle"></i> {{ session()->get('error') }}
        </div>
    @endif
@stop

@section('content')
	<div class="card card-info">
		<div class="card-header">
		<h3 class="card-title">Cambiar Contraseña</h3>
		</div>
		<!-- /.card-header -->
		<!-- form start -->
		<form role="form" class="form-horizontal" id="form" method="POST" action="{{action('UsuarioController@storePassword')}}">
			{{ csrf_field() }}
			<div class="card-body">
				<div class="form-group">
					<div class="row">
						<div class="col-sm-4">
							<label>Rut: </label>
							<label>{{$usuario->rut}}</label>
						</div>
						<div class="col-sm-4">
							<label>Nombre: </label>
							<label>{{$usuario->nombre_completo}}</label>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-sm-4">
							<label for="password_actual">Contraseña Actual</label>
							<input type="password" class="form-control" id="password_actual" name="password_actual" required>
						</div>
						<div class="col-sm-4">
							<label for="password_nueva">Contraseña Nueva</label>
							<input type="password" class="form-control" id="password_nueva" name="password_nueva" required>
						</div>
						<div class="col-sm-4">
							<label for="password_nueva_2">Repita Contraseña Nueva</label>
							<input type="password" class="form-control" id="password_nueva_2" name="password_nueva_2" required>
						</div>
					</div>
				</div>
			</div>
		  	<div class="card-footer text-right">
				<button type="submit" class="btn btn-info">Crear</button>
		  	</div>
		</form>
	  </div>
@stop

@section('js')
<script>
	$(".alert-success").fadeTo(20000, 500).slideUp(500, function(){
        $(".alert-success").slideUp(1000);
    });

    $(".alert-danger").fadeTo(20000, 5000).slideUp(500, function(){
        $(".alert-danger").slideUp(1000);
    });
</script>
@stop