@extends('adminlte::page')

@section('title', 'Ingresar Referencia Odontologica')

@section('content')
	<div class="card card-info">
		<div class="card-header">
		<h3 class="card-title">Registrar Referencia Odontologica</h3>
		</div>
		<!-- /.card-header -->
		<!-- form start -->
		<form role="form" class="form-horizontal" id="form" method="POST" action="{{action('ReferenciaOdontologicaController@store')}}" accept-charset="UTF-8" enctype="multipart/form-data">
			{{ csrf_field() }}
			<input type="hidden" id="id" name="id" value="{{isset($referenciaOdontologica) ? $referenciaOdontologica->id : ''}}"/>
			<input type="hidden" id="id_estado" name="id_estado"/>
			<input type="hidden" id="consentimiento_informado" name="consentimiento_informado"/>
			<div class="card-body">
				<h4>Datos del Paciente</h4>
				<hr>
				<div class="form-group">
					<div class="row">
						<div class="col-sm-4">
							<div class="row">
								<div class="col-sm-2">
									<label>Rut</label>
								</div>
								<div class="col-sm-1">
									<input class="form-check-input" type="radio" id="id_tipo_identificacion_paciente" name="id_tipo_identificacion_paciente" value="1" onclick="getPerson($('#rut').val(), $('#id_tipo_identificacion_paciente:checked').val());" {{isset($referenciaOdontologica->paciente->id_tipo_identificacion_paciente) && $referenciaOdontologica->paciente->id_tipo_identificacion_paciente == 1 ? "checked" : "checked"}}>
								</div>
								<div class="col-sm-4">
									<label>Pasaporte</label>
								</div>
								<div class="col-sm-1">
									<input class="form-check-input" type="radio" id="id_tipo_identificacion_paciente" name="id_tipo_identificacion_paciente" value="2" onclick="getPerson($('#rut').val(), $('#id_tipo_identificacion_paciente:checked').val());" {{isset($referenciaOdontologica->paciente->id_tipo_identificacion_paciente) && $referenciaOdontologica->paciente->id_tipo_identificacion_paciente == 2 ? "checked" : ""}}>
								</div>
							</div>
						<input type="text" class="form-control" id="rut" name="rut" onblur="getPerson($(this).val(), $('#id_tipo_identificacion_paciente:checked').val());" value="{{isset($referenciaOdontologica->paciente->rut) ? $referenciaOdontologica->paciente->rut : ""}}" required>
						</div>
						<div class="col-sm-4">
							<label for="fc_nacimiento">Fecha de Nacimiento<span style="color:#FF0000";>*</span></label>
							<div class="input-group">
								<div class="input-group-prepend">
									<span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
								</div>
								<input type="date" class="form-control" id="fc_nacimiento" name="fc_nacimiento" value="{{isset($referenciaOdontologica->paciente->fc_nacimiento) ? $referenciaOdontologica->paciente->fc_nacimiento : ""}}" disabled required>
							</div>						
						</div>
						<div class="col-sm-4">
							<label for="id_sexo">Sexo<span style="color:#FF0000";>*</span></label>
							<select class="form-control" id="id_sexo" name="id_sexo" disabled required>
								<option value="">Seleccione Sexo</option>
								<option value="1" {{isset($referenciaOdontologica->paciente->id_sexo) && $referenciaOdontologica->paciente->id_sexo == 1 ? "selected" : ""}}>Masculino</option>
								<option value="2" {{isset($referenciaOdontologica->paciente->id_sexo) && $referenciaOdontologica->paciente->id_sexo == 2 ? "selected" : ""}}>Femenino</option>
							</select>
						</div>
					</div>
				</div>

				<div class="form-group">
					<div class="row">
						<div class="col-sm-4">
							<label for="tx_nombre">Nombre<span style="color:#FF0000";>*</span></label>
							<input type="text" class="form-control" id="tx_nombre" name="tx_nombre" value="{{isset($referenciaOdontologica->paciente->tx_nombre) ? $referenciaOdontologica->paciente->tx_nombre : ""}}" readonly required>
						</div>
						<div class="col-sm-4">
							<label for="tx_apellido_paterno">A. Paterno<span style="color:#FF0000";>*</span></label>
							<input type="text" class="form-control" id="tx_apellido_paterno" name="tx_apellido_paterno" value="{{isset($referenciaOdontologica->paciente->tx_apellido_paterno) ? $referenciaOdontologica->paciente->tx_apellido_paterno : ""}}" readonly required>
						</div>
						<div class="col-sm-4">
							<label for="tx_apellido_materno">A. Materno<span style="color:#FF0000";>*</span></label>
							<input type="text" class="form-control" id="tx_apellido_materno" name="tx_apellido_materno" value="{{isset($referenciaOdontologica->paciente->tx_apellido_materno) ? $referenciaOdontologica->paciente->tx_apellido_materno : ""}}" readonly required>
						</div>
					</div>
				</div>

				<div class="form-group">
					<div class="row">
						<div class="col-sm-4">
							<label for="tx_direccion">Dirección<span style="color:#FF0000";>*</span></label>
							<input type="text" class="form-control" id="tx_direccion" name="tx_direccion" value="{{isset($referenciaOdontologica->paciente->tx_direccion) ? $referenciaOdontologica->paciente->tx_direccion : ""}}" required>
						</div>
						<div class="col-sm-4">
							<label for="tx_telefono">Teléfono/Celular<span style="color:#FF0000";>*</span></label>
							<input type="text" class="form-control" id="tx_telefono" name="tx_telefono" value="{{isset($referenciaOdontologica->paciente->tx_telefono) ? $referenciaOdontologica->paciente->tx_telefono : ""}}" required>
						</div>
						<div class="col-sm-4">
							<label for="id_prevision">Previsión<span style="color:#FF0000";>*</span></label>
							<select class="form-control" id="id_prevision" name="id_prevision" disabled required>
								<option value="">Seleccione Previsión</option>
								@foreach ($clasificacionesFonasa as $clasificacionFonasa)
									<option value={{$clasificacionFonasa->cd_clasificacion_fonasa}} {{isset($referenciaOdontologica->paciente->id_clasificacion_fonasa) && $referenciaOdontologica->paciente->id_clasificacion_fonasa == $clasificacionFonasa->id ? "selected" : ""}}>FONASA {{$clasificacionFonasa->tx_descripcion}}</option>								
								@endforeach
								@foreach ($previsiones as $prevision)
									@if($prevision->tx_descripcion != "FONASA")
										<option value={{$prevision->id}} {{isset($referenciaOdontologica->paciente->id_prevision) && $referenciaOdontologica->paciente->id_prevision != 1 && $referenciaOdontologica->paciente->id_prevision == $prevision->id ? "selected" : ""}}>{{$prevision->tx_descripcion}}</option>								
									@endif
								@endforeach
							</select>
						</div>
					</div>
				</div>

				<div class="form-group">
					<div class="row">
						<div class="col-sm-4">
							<label>Comuna<span style="color:#FF0000";>*</span></label>
							<select class="form-control"  id="id_comuna_paciente" name="id_comuna_paciente" required>
								<option value="">Seleccione Comuna</option>
								@foreach ($comunas as $comuna)
									<option value={{$comuna->cd_comuna}} {{isset($referenciaOdontologica->establecimiento->comuna->id) && $referenciaOdontologica->establecimiento->comuna->id == $comuna->id ? "selected" : ""}}>{{$comuna->tx_descripcion}}</option>								
								@endforeach
							</select>
						</div>
					</div>
				</div>

				<br>

				<h4>Datos del Profesional</h4>
				<hr>
				<div class="form-group">
					<div class="row">
						<div class="col-sm-4">
							<label>Rut</label>
							<input type="text" class="form-control" value="{{Auth::user()->rut}}" readonly>
						</div>
						<div class="col-sm-4">
							<label for="id_establecimiento">Establecimiento<span style="color:#FF0000";>*</span></label>
							<select class="form-control" id="id_establecimiento" name="id_establecimiento" onchange="comuna($(this).val())" required>
								<option value="">Seleccione Establecimiento</option>
								@if(count($establecimientos) < 2)
									@foreach ($establecimientos as $establecimiento)
										<option value={{$establecimiento->id}} selected>{{$establecimiento->tx_descripcion}}</option>								
									@endforeach
								@else
									@foreach ($establecimientos as $establecimiento)
										<option value={{$establecimiento->id}} {{isset($referenciaOdontologica->id_establecimiento) && $referenciaOdontologica->id_establecimiento == $establecimiento->id ? "selected" : ""}}>{{$establecimiento->tx_descripcion}}</option>								
									@endforeach
								@endif
							</select>
						</div>
						<div class="col-sm-4">
							<label>Comuna</label>
							<select class="form-control"  id="id_comuna" name="id_comuna" disabled>
								<option value="">Seleccione Comuna</option>
								@foreach ($comunas as $comuna)
									<option value={{$comuna->id}} {{isset($referenciaOdontologica->establecimiento->comuna->id) && $referenciaOdontologica->establecimiento->comuna->id == $comuna->id ? "selected" : ""}}>{{$comuna->tx_descripcion}}</option>								
								@endforeach
							</select>
						</div>
					</div>
				</div>

				<div class="form-group">
					<div class="row">
						<div class="col-sm-4">
							<label>Nombre</label>
							<input type="text" class="form-control" value={{Auth::user()->tx_nombre}} readonly>
						</div>
						<div class="col-sm-4">
							<label>A. Paterno</label>
							<input type="text" class="form-control" value={{Auth::user()->tx_apellido_paterno}} readonly>
						</div>
						<div class="col-sm-4">
							<label>A. Materno</label>
							<input type="text" class="form-control" value={{Auth::user()->tx_apellido_materno}} readonly>
						</div>
					</div>
				</div>

				<br>

				<h4>Antecedentes Clínicos</h4>
				<hr>
				<div class="form-group">
					<div class="row">
						<div class="col-sm-6">
							<label for="id_cie10">Diagnostico Cie 10<span style="color:#FF0000";>*</span></label>
							<select class="form-control" id="id_cie10" name="id_cie10">
							</select>
						</div>
						<div class="col-sm-2">
							<label for="bo_cancer">Sospecha de Cancer<span style="color:#FF0000";>*</span></label>
							<div class="row">
								<div class="col-sm-3">
									<label>Sí</label>
								</div>
								<div class="col-sm-1">
									<input class="form-check-input" type="radio" id="bo_cancer" name="bo_cancer" value="1" {{isset($referenciaOdontologica->bo_cancer) && $referenciaOdontologica->bo_cancer == 1 ? "checked" : ""}}>
								</div>
								<div class="col-sm-3">
									<label>No</label>
								</div>
								<div class="col-sm-1">
									<input class="form-check-input" type="radio" id="bo_cancer" name="bo_cancer" value="0" {{isset($referenciaOdontologica->bo_cancer) && $referenciaOdontologica->bo_cancer == 0 ? "checked" : ""}}>
								</div>
							</div>
						</div>
						<div class="col-sm-4">
							<label for="prioridad">Prioridad<span style="color:#FF0000";>*</span></label>
							<div class="row">
								<div class="col-sm-3">
									<label title="Sospecha de cáncer oral y/o lesiones ulcerativas extensas en mucosa oral con dolor según escala visual análoga (EVA) mayor a 4">Alta</label>
								</div>
								<div class="col-sm-1">
									<input class="form-check-input" type="radio" id="prioridad" name="prioridad" value="1" {{isset($referenciaOdontologica->prioridad) && $referenciaOdontologica->prioridad == 1 ? "checked" : ""}}>
								</div>
								<div class="col-sm-3">
									<label title="Sospecha de desórdenes potencialmente malignos, neoplasias benignas, tumores óseos">Media</label>
								</div>
								<div class="col-sm-1">
									<input class="form-check-input" type="radio" id="prioridad" name="prioridad" value="2" {{isset($referenciaOdontologica->prioridad) && $referenciaOdontologica->prioridad == 2 ? "checked" : ""}}>
								</div>
								<div class="col-sm-3">
									<label title="Lesiones reactivas o traumáticas (como fibromas irritativos, mucocele, etc.) o infecciones de baja complejidad (papilomas, verrugas, candidiasis, etc.)">Baja</label>
								</div>
								<div class="col-sm-1">
									<input class="form-check-input" type="radio" id="prioridad" name="prioridad" value="3" {{isset($referenciaOdontologica->prioridad) && $referenciaOdontologica->prioridad == 3 ? "checked" : ""}}>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="form-group">
					<div class="row">
						<div class="col-sm-6">
							<label for="sospecha_diagnostica">Sospecha Diagnóstica<span style="color:#FF0000";>*</span></label>
							<textarea class="form-control noresize" id="sospecha_diagnostica" name="sospecha_diagnostica">{{isset($referenciaOdontologica->sospecha_diagnostica) ? $referenciaOdontologica->sospecha_diagnostica : ""}}</textarea>
						</div>
						<div class="col-sm-6">
							<label for="motivo_consulta">Motivo de Consulta<span style="color:#FF0000";>*</span></label>
							<textarea class="form-control noresize" id="motivo_consulta" name="motivo_consulta">{{isset($referenciaOdontologica->motivo_consulta) ? $referenciaOdontologica->motivo_consulta : ""}}</textarea>
						</div>
					</div>
				</div>

				<div class="form-group">
					<div class="row">
						<div class="col-sm-6">
							<label for="antecedente_morbido">Antecedentes Mórbidos<span style="color:#FF0000";>*</span></label>
							<textarea class="form-control noresize" id="antecedente_morbido" name="antecedente_morbido">{{isset($referenciaOdontologica->antecedente_morbido) ? $referenciaOdontologica->antecedente_morbido : ""}}</textarea>
						</div>
						<div class="col-sm-6">
							<label for="alergia">Alergías<span style="color:#FF0000";>*</span></label>
							<textarea class="form-control noresize" id="alergia" name="alergia">{{isset($referenciaOdontologica->alergia) ? $referenciaOdontologica->alergia : ""}}</textarea>
						</div>
					</div>
				</div>

				<br>

				<h4>Hábitos</h4>
				<hr>
				<div class="form-group">
					<div class="row">
						<div class="col-sm-2">
							<label>Fumador<span style="color:#FF0000";>*</span></label>
							<div class="row">
								<div class="col-sm-3">
									<label>Sí</label>
								</div>
								<div class="col-sm-1">
									<input class="form-check-input" type="radio" id="id_fumador" name="id_fumador" value="1" onchange="cigarrillo($(this).val());" {{isset($referenciaOdontologica->paciente->habito->nr_cigarrillo) && $referenciaOdontologica->paciente->habito->nr_cigarrillo > 0 ? "checked" : ""}}>
								</div>
								<div class="col-sm-3">
									<label>No</label>
								</div>
								<div class="col-sm-1">
									<input class="form-check-input" type="radio" id="id_fumador" name="id_fumador" value="0" onchange="cigarrillo($(this).val());" {{isset($referenciaOdontologica->paciente->habito->nr_cigarrillo) && $referenciaOdontologica->paciente->habito->nr_cigarrillo == 0 ? "checked" : ""}}>
								</div>
							</div>
						</div>
						<div class="col-sm-2">
							<label for="nr_cigarro">Cigarrillos diarios</label>
							<div class="col-sm-6">
								<input type="number" class="form-control" id="nr_cigarrillo" name="nr_cigarrillo" min="0" value="{{isset($referenciaOdontologica->paciente->habito->nr_cigarrillo) ? $referenciaOdontologica->paciente->habito->nr_cigarrillo : ""}}" {{isset($referenciaOdontologica->paciente->habito->nr_cigarrillo) && $referenciaOdontologica->paciente->habito->nr_cigarrillo == 0 ? "readonly" : ""}}>	
							</div>				
						</div>
						<div class="col-sm-2">
							<label>Consumo de Alcohol<span style="color:#FF0000";>*</span></label>
							<div class="row">
								<div class="col-sm-3">
									<label>Sí</label>
								</div>
								<div class="col-sm-1">
									<input class="form-check-input" type="radio" id="id_consumo_alcohol" name="id_consumo_alcohol" value="1" onchange="alcohol($(this).val());" {{isset($referenciaOdontologica->paciente->habito->alcohol) && $referenciaOdontologica->paciente->habito->alcohol == 1 ? "checked" : ""}}>
								</div>
								<div class="col-sm-3">
									<label>No</label>
								</div>
								<div class="col-sm-1">
									<input class="form-check-input" type="radio" id="id_consumo_alcohol" name="id_consumo_alcohol" value="0" onchange="alcohol($(this).val());" {{isset($referenciaOdontologica->paciente->habito->alcohol) && $referenciaOdontologica->paciente->habito->alcohol == 0 ? "checked" : ""}}>
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<label for="observacion">Observaciones</label>
							<input type="text" class="form-control" id="observacion" name="observacion" value="{{isset($referenciaOdontologica->paciente->habito->observacion) ? $referenciaOdontologica->paciente->habito->observacion : ""}}" {{isset($referenciaOdontologica->paciente->habito->alcohol) && $referenciaOdontologica->paciente->habito->alcohol == 0 ? "readonly" : ""}}>					
						</div>
					</div>
				</div>

				<div class="form-group">
					<div class="row">
						<div class="col-sm-6">
							<label for="otro">Otros<span style="color:#FF0000";>*</span></label>
							<textarea class="form-control noresize" id="otro" name="otro">{{isset($referenciaOdontologica->paciente->habito->otro) ? $referenciaOdontologica->paciente->habito->otro : ""}}</textarea>
						</div>
						<div class="col-sm-6">
							<label for="medicamento">Medicamentos<span style="color:#FF0000";>*</span></label>
							<textarea class="form-control noresize" id="medicamento" name="medicamento">{{isset($referenciaOdontologica->paciente->habito->medicamento) ? $referenciaOdontologica->paciente->habito->medicamento : ""}}</textarea>
						</div>
					</div>
				</div>

				<br>

				<h4>Descripción de la Lesión</h4>
				<hr>
				<div class="form-group">
					<div class="row">
						<div class="col-sm-2">
							<label for="tamano_lesion">Tamaño lesión (mm)<span style="color:#FF0000";>*</span></label>
							<div class="col-sm-8">
								<input type="number" class="form-control" id="tamano_lesion" name="tamano_lesion" min="1" placeholder="mm" value="{{isset($referenciaOdontologica->tamano_lesion) ? $referenciaOdontologica->tamano_lesion : ""}}">	
							</div>				
						</div>
						<div class="col-sm-4">
							<label for="tm_evolucion">Tiempo de evolución<span style="color:#FF0000";>*</span></label>
							<div class="row">
								<div class="col-sm-3">
									<input type="number" class="form-control" id="tm_evolucion" name="tm_evolucion" value="{{isset($referenciaOdontologica->tm_evolucion) ? $referenciaOdontologica->tm_evolucion : ""}}">
								</div>
								<div class="col-sm-4">
									<select class="form-control" id="id_unidad_tiempo" name="id_unidad_tiempo">
										<option value="">Unidad</option>
										@foreach ($unidades_tiempo as $unidad_tiempo)
											<option value={{$unidad_tiempo->id}} {{isset($referenciaOdontologica->id_unidad_tiempo) && $referenciaOdontologica->id_unidad_tiempo == $unidad_tiempo->id ? "selected" : ""}}>{{$unidad_tiempo->nombre}}</option>								
										@endforeach
									</select>
								</div>
							</div>						
						</div>
						<div class="col-sm-4">
							<label for="sintomatologia">Sintomatología<span style="color:#FF0000";>*</span></label>
							<input type="text" class="form-control" id="sintomatologia" name="sintomatologia" value="{{isset($referenciaOdontologica->sintomatologia) ? $referenciaOdontologica->sintomatologia : ""}}">
						</div>
					</div>
				</div>

				<div class="form-group">
					<div class="row">
						<div class="col-sm-12">
							<label for="otro_lesion">Otros (Forma, color, consistencia, etc)<span style="color:#FF0000";>*</span></label>
							<textarea class="form-control noresize" id="otro_lesion" name="otro_lesion">{{isset($referenciaOdontologica->otro) ? $referenciaOdontologica->otro : ""}}</textarea>
						</div>
					</div>
				</div>

				<h4>Ubicación</h4>
				<hr>
				<div class="form-group">
					<div class="row">
						<div class="col-sm-6">
							<label>Lesión Mucosa</label>
							<select class="select2 select2-hidden-accessible" id="mucosa" name="mucosa[]" multiple="" data-placeholder="Seleccione pieza(s)" style="width: 100%;" data-select2-id="7" tabindex="-1" aria-hidden="true">
								@foreach ($mucosas as $mucosa)
									<option value={{$mucosa->id}} {{isset($referenciaOdontologica->mucosas) && in_array($mucosa->id, $referenciaOdontologica->mucosas->pluck('id')->toArray()) ? 'selected' : ''}}>{{$mucosa->id}}</option>
								@endforeach
							</select>
							<img src="{{url('/images/mucosa.jpg')}}">
						</div>
						<div class="col-sm-6">
							<label>Lesión Ósea</label>
							<select class="select2 select2-hidden-accessible" id="osea" name="osea[]" multiple="" data-placeholder="Seleccione pieza(s)" style="width: 100%;" data-select2-id="7" tabindex="-1" aria-hidden="true">
								@foreach ($oseas as $osea)
									<option value={{$osea->id}} {{isset($referenciaOdontologica->oseas) && in_array($osea->id, $referenciaOdontologica->oseas->pluck('id')->toArray()) ? 'selected' : ''}}>{{$osea->id}}</option>
								@endforeach
							</select>
							<img src="{{url('/images/osea.png')}}">
						</div>
					</div>
				</div>

				<h4>Archivos
					<h6 style="color:red">@if(isset($referenciaOdontologica))
						Si desea reemplazar un archivo subido anteriormente (con ticket a la derecha), puede volver a seleccionar otro.
					@endif</h6>
				</h4>
				<hr>
				<div class="form-group row">
					<div class="col-sm-4">
						<label>Consentimiento Informado<span style="color:#FF0000";>*</span></label>
					</div>
					<div class="col-sm-5">
						<input accept="image/jpeg,image/png" type="file" class="form-control" id="consentimiento" name="consentimiento">
					</div>
					@if(isset($referenciaOdontologica->consentimiento))
						<div class="col-sm-1">
							<a href="/storage/{{$referenciaOdontologica->consentimiento}}" target="_blank" class="btn btn-success btn-xs"><i class="fa fa-check" style="color:white"></i></a>
						</div>
					@endif
					<div class="col-sm-2">
						<button type="submit" class="btn btn-warning" title="Se creara un borrador" onclick="ci()">Hacer Consentimiento</button>
					</div>
				</div>
				<div class="form-group row">
					<div class="col-sm-4">
						<label>Adjuntar Fotografía 1<span style="color:#FF0000";>*</span></label>
					</div>
					<div class="col-sm-5">
						<input accept="image/jpeg,image/png" type="file" class="form-control" id="foto_1" name="foto_1">
					</div>
					@if(isset($referenciaOdontologica->foto_1))
						<div class="col-sm-1">
							<a href="/storage/{{$referenciaOdontologica->foto_1}}" target="_blank" class="btn btn-success btn-xs"><i class="fa fa-check" style="color:white"></i></a>
						</div>
					@endif
				</div>
				<div class="form-group row">
					<div class="col-sm-4">
						<label>Adjuntar Fotografía 2</label>
					</div>
					<div class="col-sm-5">
						<input accept="image/jpeg,image/png" type="file" class="form-control" id="foto_2" name="foto_2">
					</div>
					@if(isset($referenciaOdontologica->foto_2))
						<div class="col-sm-1">
							<a href="/storage/{{$referenciaOdontologica->foto_2}}" target="_blank" class="btn btn-success btn-xs"><i class="fa fa-check" style="color:white"></i></a>
						</div>
					@endif
				</div>
		  	</div>
		  <!-- /.card-body -->

		  	<div class="card-footer text-right">
				<button type="submit" class="btn btn-warning" onclick="borrador()">Guardar Borrador</button>
				<button type="submit" class="btn btn-info" onclick="enviar($('#mucosa').val(), $('#osea').val())">Enviar</button>
		  	</div>
		</form>
	  </div>
@stop

@section('js')
<script>
	@if(request()->ci == 1)
		var id = {{request()->id}};
		window.open('/pdfConsentimientoInformado?id='+id);
	@endif
	var url_diagnosticos = @json(url('getDiagnostico'));

	function ci(){
		$("#id_estado").val(0);
		$("#consentimiento_informado").val(1);
		$("#fc_nacimiento").css('display','').attr('disabled', false);
		$("#id_sexo").css('display','').attr('disabled', false);
		$("#id_prevision").css('display','').attr('disabled', false);
		$("#nr_cigarrillo").css('display','').attr('disabled', false);
		$("#id_cie10").css('display','').attr('required', false);
		$("#bo_cancer").css('display','').attr('required', false);
		$("#prioridad").css('display','').attr('required', false);
		$("#sospecha_diagnostica").css('display','').attr('required', false);
		$("#motivo_consulta").css('display','').attr('required', false);
		$("#antecedente_morbido").css('display','').attr('required', false);
		$("#alergia").css('display','').attr('required', false);
		$("#tamano_lesion").css('display','').attr('required', false);
		$("#tm_evolucion").css('display','').attr('required', false);
		$("#id_unidad_tiempo").css('display','').attr('required', false);
		$("#sintomatologia").css('display','').attr('required', false);
		$("#otro_lesion").css('display','').attr('required', false);
		$("#consentimiento").css('display','').attr('required', false);
		$("#foto_1").css('display','').attr('required', false);
		$("#id_fumador").css('display','').attr('required', false);
		$("#id_consumo_alcohol").css('display','').attr('required', false);
	}

	function borrador(){
		$("#id_estado").val(0);
		$("#fc_nacimiento").css('display','').attr('disabled', false);
		$("#id_sexo").css('display','').attr('disabled', false);
		$("#id_prevision").css('display','').attr('disabled', false);
		$("#nr_cigarrillo").css('display','').attr('disabled', false);
		$("#id_cie10").css('display','').attr('required', false);
		$("#bo_cancer").css('display','').attr('required', false);
		$("#prioridad").css('display','').attr('required', false);
		$("#sospecha_diagnostica").css('display','').attr('required', false);
		$("#motivo_consulta").css('display','').attr('required', false);
		$("#antecedente_morbido").css('display','').attr('required', false);
		$("#alergia").css('display','').attr('required', false);
		$("#tamano_lesion").css('display','').attr('required', false);
		$("#tm_evolucion").css('display','').attr('required', false);
		$("#id_unidad_tiempo").css('display','').attr('required', false);
		$("#sintomatologia").css('display','').attr('required', false);
		$("#otro_lesion").css('display','').attr('required', false);
		$("#consentimiento").css('display','').attr('required', false);
		$("#foto_1").css('display','').attr('required', false);
		$("#id_fumador").css('display','').attr('required', false);
		$("#id_consumo_alcohol").css('display','').attr('required', false);
	}

	function enviar(mucosa, osea){
		if(mucosa == "" && osea == ""){
			alert("Debe tener al menos una lesión Mucosa/Osea");
			$("#mucosa").css('display','').attr('required', true);
			$("#osea").css('display','').attr('required', true);
		}else{
			$("#mucosa").css('display','').attr('required', false);
			$("#osea").css('display','').attr('required', false);
		}
		$("#id_estado").val(1);
		$("#fc_nacimiento").css('display','').attr('disabled', false);
		$("#id_sexo").css('display','').attr('disabled', false);
		$("#id_prevision").css('display','').attr('disabled', false);
		$("#nr_cigarrillo").css('display','').attr('disabled', false);
		$("#id_cie10").css('display','').attr('required', true);
		$("#bo_cancer").css('display','').attr('required', true);
		$("#prioridad").css('display','').attr('required', true);
		$("#sospecha_diagnostica").css('display','').attr('required', true);
		$("#motivo_consulta").css('display','').attr('required', true);
		$("#antecedente_morbido").css('display','').attr('required', true);
		$("#alergia").css('display','').attr('required', true);
		$("#tamano_lesion").css('display','').attr('required', true);
		$("#tm_evolucion").css('display','').attr('required', true);
		$("#id_unidad_tiempo").css('display','').attr('required', true);
		$("#sintomatologia").css('display','').attr('required', true);
		$("#otro_lesion").css('display','').attr('required', true);
		if(@json(isset($referenciaOdontologica->consentimiento) == false)){
			$("#consentimiento").css('display','').attr('required', true);
		}
		if(@json(isset($referenciaOdontologica->foto_1) == false)){
			$("#foto_1").css('display','').attr('required', true);
		}
		$("#id_fumador").css('display','').attr('required', true);
		$("#id_consumo_alcohol").css('display','').attr('required', true);
	}

	function getPerson(rut, id_tipo_identificacion_paciente){
		if(id_tipo_identificacion_paciente == 1){
			$identificacion = "rut";
		}else{
			$identificacion = "pasaporte";
		}
		$.getJSON("{{action('PacienteController@getDatosRut')}}?"+$identificacion+"="+rut,
		function(data){
			if(data.encontrado){
				$("#fc_nacimiento").val(data.fc_nacimiento);
				$("#fc_nacimiento").css('display','').attr('disabled', true);
				$("#id_sexo").val(data.id_sexo);
				$("#id_sexo").css('display','').attr('disabled', true);
				$("#tx_nombre").val(data.tx_nombre);
				$("#tx_nombre").css('display','').attr('readonly', true);
				$("#tx_apellido_paterno").val(data.tx_apellido_paterno);
				$("#tx_apellido_paterno").css('display','').attr('readonly', true);
				$("#tx_apellido_materno").val(data.tx_apellido_materno);
				$("#tx_apellido_materno").css('display','').attr('readonly', true);
				$("#tx_direccion").val(data.tx_direccion);
				$("#tx_telefono").val(data.tx_telefono);
				$("#id_comuna_paciente").val(data.cdgComuna);
				if(data.id_prevision != 1){
					$("#id_prevision").val(data.id_prevision);
					$("#id_prevision").css('display','').attr('disabled', true);
				}else if (data.id_prevision == 1 && data.id_clasificacion_fonasa > 0){
					if(data.id_clasificacion_fonasa == 1){
						$("#id_prevision").val("A");
					}else if(data.id_clasificacion_fonasa == 2){
						$("#id_prevision").val("B");
					}else if(data.id_clasificacion_fonasa == 3){
						$("#id_prevision").val("C");
					}else if(data.id_clasificacion_fonasa == 4){
						$("#id_prevision").val("D");
					}
					$("#id_prevision").css('display','').attr('disabled', true);
				}else{
					$("#id_prevision").val("");
					$("#id_prevision").css('display','').attr('disabled', false);
				}
			}else{
				if(id_tipo_identificacion_paciente == 1){
					alert("Rut no encontrado");
					$("#rut").val("");
					$("#id_sexo").val("");
					$("#tx_nombre").val("");
					$("#tx_apellido_paterno").val("");
					$("#tx_apellido_materno").val("");
					$("#tx_direccion").val("");
					$("#tx_telefono").val("");
					$("#id_prevision").val("");
				}
				$("#fc_nacimiento").css('display','').attr('disabled', false);
				$("#id_sexo").css('display','').attr('disabled', false);
				$("#tx_nombre").css('display','').attr('readonly', false);
				$("#tx_apellido_paterno").css('display','').attr('readonly', false);
				$("#tx_apellido_materno").css('display','').attr('readonly', false);
				$("#id_prevision").css('display','').attr('disabled', false);
			}
		})
	};

	function comuna(id){
		$.getJSON("{{action('PacienteController@getComunaEstablecimiento')}}?id_establecimiento="+id,
		function(data){
			$("#id_comuna").val(data);
		})
	};

	function cigarrillo(id){
		if(id == 0){
			$("#nr_cigarrillo").val(0);
			$("#nr_cigarrillo").css('display','').attr('disabled', true);
			$("#nr_cigarrillo").css('display','').attr('min', 0);
		}else{
			$("#nr_cigarrillo").val(1);
			$("#nr_cigarrillo").css('display','').attr('disabled', false);
			$("#nr_cigarrillo").css('display','').attr('min', 1);
		}
	};

	function alcohol(id){
		if(id == 0){
			$("#observacion").val("");
			$("#observacion").css('display','').attr('disabled', true);
			$("#observacion").css('display','').attr('required', false);
		}else{
			$("#observacion").css('display','').attr('disabled', false);
			$("#observacion").css('display','').attr('required', true);
		}
	};

	$('#id_cie10').select2({
        width: '99%',
        allowClear: true,
        minimumInputLength: 3,
        placeholder: "Seleccione CIE10",
        language: {
            inputTooShort: function() {
                return 'Ingrese 3 o más caracteres para la búsqueda';
            }
        },
        ajax: {
            url: url_diagnosticos,
            dataType: 'json',
            data: function (params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            }
        }
	})

	@if(!empty($referenciaOdontologica->cie10))
        var cie10_array = [];
		var newOption1 = new Option('{{ $referenciaOdontologica->cie10->cd_diagnostico}} {{ $referenciaOdontologica->cie10->tx_descripcion}}', {{ $referenciaOdontologica->cie10->id }}, true, true);
		cie10_array.push(newOption1);
        $('#id_cie10').append(cie10_array).trigger('change');
    @endif
	
	$('#mucosa').select2();
	$('#osea').select2();
</script>
@stop


