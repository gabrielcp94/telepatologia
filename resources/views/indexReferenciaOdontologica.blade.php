@extends('adminlte::page')

@section('title', 'Mis Referencias Odontologicas')

@section('content_header')
    @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @endif
    @if(session()->has('error'))
        <div class="alert alert-danger">
            <i class="fa fa-exclamation-triangle"></i> {{ session()->get('error') }}
        </div>
    @endif
@stop

@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Mis Referencias Odontologicas</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <div id="example1_wrapper" class="dataTables_wrapper dt-bootstrap4">
                <div class="row">
                    <div class="col-sm-12">
                        <form class="form-horizontal">
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <select class="form-control" id="id_profesional" name="id_profesional" {{Auth::user()->id_perfil == 3 ? 'disabled' : ''}}>
                                        <option value="">Seleccione Profesional</option>
                                        @foreach ($profesionales as $profesional)
                                            <option value={{$profesional->id}} {{request()->id_profesional == $profesional->id ? 'selected' : ''}}>{{$profesional->nombre_completo}}</option>								
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <select class="form-control" id="id_establecimiento" name="id_establecimiento">
                                        <option value="">Seleccione Establecimiento</option>
                                        @if(count($establecimientos) < 2)
                                            @foreach ($establecimientos as $establecimiento)
                                                <option value={{$establecimiento->id}} selected>{{$establecimiento->tx_descripcion}}</option>								
                                            @endforeach
                                        @else
                                            @foreach ($establecimientos as $establecimiento)
                                                <option value={{$establecimiento->id}} {{request()->id_establecimiento == $establecimiento->id ? 'selected' : ''}}>{{$establecimiento->tx_descripcion}}</option>								
                                            @endforeach
                                        @endif
                                    </select>                                
                                </div>
                                <div class="col-sm-2">
                                    <div class="custom-control custom-checkbox">
                                        <input class="custom-control-input" type="checkbox" id="bo_cancer" name="bo_cancer" value="1" {{request()->bo_cancer == 1 ? 'checked' : ''}}>
                                        <label for="bo_cancer" class="custom-control-label">S. de Cancer</label>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="custom-control custom-checkbox">
                                        <input class="custom-control-input" type="checkbox" id="id_estado" name="id_estado" value="1" {{request()->id_estado == 1 ? 'checked' : ''}}>
                                        <label for="id_estado" class="custom-control-label">Resuelta</label>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <button type="submit" class="btn btn-info">Filtrar</button>
                                </div>
                            </div>
                        </form>
                        <table class="table table-striped table-hover" id="tableUsers">
                            <thead>
                                <th>ID</th>
                                <th>Paciente</th>
                                <th>Rut/Pasaporte</th>
                                <th>Sospecha Diagnóstica</th>
                                <th>Establecimiento</th>
                                <th>Profesional</th>
                                <th>Estado</th>
                                <th>Fecha Solicitud</th>
                            </thead>
                            <tbody>
                                @foreach ($referenciasOdontologicas as $referenciaOdontologica)
                                    <tr role="row" class="odd">
                                        <td>{{$referenciaOdontologica->id}}</td>
										@if($referenciaOdontologica->paciente['id_tipo_identificacion_paciente'] == 1)
                                            <td><a href="paciente?rut={{$referenciaOdontologica->paciente['rut']}}">{{$referenciaOdontologica->paciente['nombre_completo']}}<a></td>
                                            <td>{{$referenciaOdontologica->paciente['rut']}}</td>
                                        @else
                                            <td><a href="paciente?pasaporte={{$referenciaOdontologica->paciente['tx_pasaporte']}}">{{$referenciaOdontologica->paciente['nombre_completo']}}<a></td>
                                            <td>{{$referenciaOdontologica->paciente['tx_pasaporte']}}</td>
                                        @endif
                                        <td>{{$referenciaOdontologica->sospecha_diagnostica}}</td>
                                        <td>{{$referenciaOdontologica->establecimiento->tx_descripcion}}</td>
                                        <td>{{$referenciaOdontologica->profesional->nombre_completo}}</td>
                                        <td><small class="badge badge-{{$referenciaOdontologica->estado->clase}}">{{$referenciaOdontologica->estado->nombre}}</small>
                                            @if($referenciaOdontologica->bo_cancer == 1)
                                                <small class="badge badge-danger">S. Cancer</small>
                                            @endif
                                        </td>
                                        <td>{{date('d/m/Y', strtotime(str_replace("/",".",$referenciaOdontologica->created_at)))}}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ $referenciasOdontologicas->appends(request()->query())->links() }}
                    </div>
                </div>
            </div>
        </div>
        <!-- /.card-body -->
    </div>
@stop

@section('js')
<script>
	$(".alert-success").fadeTo(20000, 500).slideUp(500, function(){
        $(".alert-success").slideUp(1000);
    });

    $(".alert-danger").fadeTo(20000, 5000).slideUp(500, function(){
        $(".alert-danger").slideUp(1000);
    });
</script>
@endsection