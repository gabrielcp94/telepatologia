@extends('adminlte::page')

@section('title', 'Editar Usuario')

@section('content')
	<div class="card card-info">
		<div class="card-header">
		<h3 class="card-title">Editar Usuario</h3>
		</div>
		<!-- /.card-header -->
		<!-- form start -->
		<form role="form" class="form-horizontal" id="form" method="POST" action="{{action('UsuarioController@storeUsuario')}}">
			{{ csrf_field() }}
			<input type="hidden" id="id" name="id" value={{$usuario->id}} />	
			<div class="card-body">
				<div class="form-group">
					<div class="row">
						<div class="col-sm-4">
							<label>Rut: </label>
							<label>{{$usuario->rut}}</label>
						</div>
						<div class="col-sm-4">
							<label>Nombre: </label>
							<label>{{$usuario->nombre_completo}}</label>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-sm-4">
							<label for="id_perfil">Perfil</label>
							<select class="form-control" id="id_perfil" name="id_perfil" onchange="perfil((this.value))" required>
								<option value="">Seleccione Perfil</option>
								@foreach ($perfiles as $perfil)
									<option value={{$perfil->id}} {{$usuario->id_perfil == $perfil->id ? 'selected' : ''}} onblur="perfil($(this).val();">{{$perfil->nombre}}</option>								
								@endforeach
							</select>
						</div>
						<div class="col-sm-4">
							<label for="establecimientos">Establecimiento</label>
							<select class="select2 select2-hidden-accessible" id="establecimientos" name="establecimientos[]" multiple="" data-placeholder="Seleccione establecimiento(s)" style="width: 100%;" data-select2-id="7" tabindex="-1" aria-hidden="true" {{$usuario->id_perfil == 2 || $usuario->id_perfil == 4 ? 'disabled' : 'required'}}>
								@foreach ($establecimientos as $establecimiento)
									<option value={{$establecimiento->id}} >{{$establecimiento->tx_descripcion}}</option>
								@endforeach
							</select>
						</div>
						<div class="col-sm-4">
							<label for="tx_email">Email</label>
						<input type="email" class="form-control" id="tx_email" name="tx_email" value="{{$usuario->tx_email}}" required>
						</div>
					</div>
				</div>
			</div>
		  	<div class="card-footer text-right">
				<button type="submit" class="btn btn-info">Guardar Cambios</button>
		  	</div>
		</form>
	  </div>
@stop

@section('js')
<script>
	var user = @json($usuario);
	var establecimientos = user.establecimientos.map(establecimientos => establecimientos['id']);

	$(".select2").val(establecimientos).trigger('change');

	function perfil(id_perfil){
		if(id_perfil == 3 || id_perfil == 1){
			$("#establecimientos").css('display','').attr('disabled', false);
			$("#establecimientos").css('display','').attr('required', true);
		}else{
			$("#establecimientos").css('display','').attr('disabled', true);
			$("#establecimientos").css('display','').attr('required', false);
			$(".select2").val('').trigger('change');
		}
	}

	$('.select2').select2();
</script>
@stop