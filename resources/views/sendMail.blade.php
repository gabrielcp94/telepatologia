@extends('adminlte::page')

@section('title', 'Enviar Correo')

@section('content_header')
    @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @endif
    @if(session()->has('error'))
        <div class="alert alert-danger">
            <i class="fa fa-exclamation-triangle"></i> {{ session()->get('error') }}
        </div>
    @endif
@stop

@section('content')
	<div class="card card-info">
		<div class="card-header">
		<h3 class="card-title">Enviar Correo</h3>
		</div>
		<!-- /.card-header -->
		<!-- form start -->
		<form role="form" class="form-horizontal" id="form" method="POST" action="{{action('UsuarioController@storeMail')}}">
			{{ csrf_field() }}
			<input type="hidden" id="emisor" name="emisor" value="{{$usuario->nombre_completo}}">	
			<input type="hidden" id="email" name="email" value="{{$usuario->tx_email}}">	
			<div class="card-body">
				<div class="form-group">
					<div class="row">
						<div class="col-sm-4">
							<label>De: </label>
							<label>{{$usuario->nombre_completo}}</label>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-sm-12">
							<label for="receptor">Para</label>
							<select class="select2 select2-hidden-accessible" id="receptor" name="receptor[]" multiple="" data-placeholder="Seleccione receptor(es)" style="width: 100%;" data-select2-id="7" tabindex="-1" aria-hidden="true">
								@foreach ($usuarios as $receptor)
									<option value={{$receptor->tx_email}} >{{$receptor->nombre_completo}} ({{$receptor->tx_email}})</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-sm-12">
							<label for="mensaje">Mensaje</label>
							<textarea type="text" class="form-control" id="mensaje" name="mensaje" required></textarea>
						</div>
					</div>
				</div>
			</div>
		  	<div class="card-footer text-right">
				<button type="submit" class="btn btn-info">Enviar</button>
		  	</div>
		</form>
	  </div>
@stop

@section('js')
<script>
	$(".alert-success").fadeTo(20000, 500).slideUp(500, function(){
        $(".alert-success").slideUp(1000);
    });

    $(".alert-danger").fadeTo(20000, 5000).slideUp(500, function(){
        $(".alert-danger").slideUp(1000);
    });

	$('.select2').select2();
</script>
@stop