@extends('adminlte::page')

@section('title', 'Crear Usuario')

@section('content')
		{{-- <form class="form-horizontal" id="form" method="POST">
			{{ csrf_field() }} --}}
			

	<div class="card card-info">
		<div class="card-header">
		<h3 class="card-title">Crear Usuario</h3>
		</div>
		<!-- /.card-header -->
		<!-- form start -->
		<form role="form" class="form-horizontal" id="form" method="POST" action="{{action('UsuarioController@store')}}">
			{{ csrf_field() }}
			<div class="card-body">
				<div class="form-group">
					<div class="row">
						<div class="col-sm-4">
							<label for="rut">Rut</label>
							<input type="text" class="form-control" id="rut" name="rut" onblur="getPerson($(this).val(), 1);" required>
						</div>
						<div class="col-sm-4">
							<label for="id_perfil">Perfil</label>
							<select class="form-control" id="id_perfil" name="id_perfil" onchange="perfil((this.value))" required>
								<option value="">Seleccione Perfil</option>
								@foreach ($perfiles as $perfil)
									<option value={{$perfil->id}} onblur="perfil($(this).val();">{{$perfil->nombre}}</option>								
								@endforeach
							</select>
						</div>
						<div class="col-sm-4">
							<label for="establecimientos">Establecimiento</label>
							<select class="select2 select2-hidden-accessible" id="establecimientos" name="establecimientos[]" multiple="" data-placeholder="Seleccione establecimiento(s)" style="width: 100%;" data-select2-id="7" tabindex="-1" aria-hidden="true" disabled>
								@foreach ($establecimientos as $establecimiento)
									<option value={{$establecimiento->id}}>{{$establecimiento->tx_descripcion}}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-sm-4">
							<label for="tx_nombre">Nombre</label>
							<input type="text" class="form-control" id="tx_nombre" name="tx_nombre" required>
						</div>
						<div class="col-sm-4">
							<label for="tx_apellido_paterno">A. Paterno</label>
							<input type="text" class="form-control" id="tx_apellido_paterno" name="tx_apellido_paterno" required>
						</div>
						<div class="col-sm-4">
							<label for="tx_apellido_materno">A. Materno</label>
							<input type="text" class="form-control" id="tx_apellido_materno" name="tx_apellido_materno" required>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-sm-4">
							<label for="tx_email">Email</label>
							<input type="email" class="form-control" id="tx_email" name="tx_email" required>
						</div>
					</div>
				</div>
			</div>
		  	<div class="card-footer text-right">
				<button type="submit" class="btn btn-info">Crear</button>
		  	</div>
		</form>
	  </div>
@stop

@section('js')
<script>
	function getPerson(rut, id_tipo_identificacion_paciente){
			$.getJSON("{{action('PacienteController@getDatosRut')}}?rut="+rut,
			function(data){
				if(data.encontrado){
					$("#tx_nombre").val(data.tx_nombre);
					$("#tx_nombre").css('display','').attr('readonly', true);
					$("#tx_apellido_paterno").val(data.tx_apellido_paterno);
					$("#tx_apellido_paterno").css('display','').attr('readonly', true);
					$("#tx_apellido_materno").val(data.tx_apellido_materno);
					$("#tx_apellido_materno").css('display','').attr('readonly', true);
				}else{
					alert("Rut no encontrado");
					$("#rut").val("");
					$("#tx_nombre").val("");
					$("#tx_nombre").css('display','').attr('readonly', false);
					$("#tx_apellido_paterno").val("");
					$("#tx_apellido_paterno").css('display','').attr('readonly', false);
					$("#tx_apellido_materno").val("");
					$("#tx_apellido_materno").css('display','').attr('readonly', false);
				}
			})
	};

	function perfil(id_perfil){
		if(id_perfil == 3){
			$("#establecimientos").css('display','').attr('disabled', false);
			$("#establecimientos").css('display','').attr('required', true);
		}else{
			$("#establecimientos").css('display','').attr('disabled', true);
			$("#establecimientos").css('display','').attr('required', false);
		}
	}

	$('.select2').select2();
</script>
@stop