<div class="modal fade" id="modal-revocacion" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Subir Revocación del Consentimineto Informado ({{$referenciaOdontologica->id}})</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="panel-body">
                    <form method="POST" action="{{action('StorageController@save')}}" accept-charset="UTF-8" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" id="id" name="id" value="{{$referenciaOdontologica->id}}">	
                        <input type="hidden" id="consentimiento" name="consentimiento" value="0">	
                        <div class="form-group row">
                            <div class="col-sm-2">
                                <label>Nuevo Archivo</label>
                            </div>
                            <div class="col-sm-7">
                                <input accept="image/jpeg,image/png" type="file" class="form-control" name="file" required>
                            </div>
                            <div class="col-sm-2">
                                <button type="submit" class="btn btn-info">Subir</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="row invoice-info">
                    <div class="col-sm-6 invoice-col">
                        <strong>Nombre: </strong>{{$paciente->nombre_completo}}<br>
                        <strong>Rut: </strong>{{$paciente->rut}}<br>
                        <strong>Fecha de Nacimiento: </strong>
                        @if($paciente->fc_nacimiento != '0000-00-00')
                            {{date('d/m/Y', strtotime(str_replace("/",".",$paciente->fc_nacimiento)))}}
                        @else 
                            Sin Información
                        @endif<br>
                        <strong>Previsión: </strong>{{$paciente->prevision->tx_descripcion}} {{$paciente->clasificacionFonasa->tx_descripcion}}
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-6 invoice-col">
                        <strong>Sexo: </strong>{{$paciente->sexo->tx_descripcion}}<br>
                        <strong>Edad: </strong>{{$paciente->edad}}<br>
                        <strong>Dirección: </strong>{{$paciente->tx_direccion}} ({{$paciente->comuna->tx_descripcion}})<br>
                        <strong>Telefono: </strong>
                        @if(isset($paciente->tx_telefono))
                            {{$paciente->tx_telefono}}
                        @else
                            Sin Información
                        @endif
                    </div>
                    <!-- /.col -->
                </div>
                <br>
                <div class="row invoice-info">
                    <div class="col-sm-6 invoice-col">
                        <strong>Fumador: </strong>
                        @if($paciente->habito->nr_cigarrillo > 0)
                            Si
                        @else
                            No
                        @endif
                        <br>
                        <strong>Consumo de Alcohol: </strong>
                        @if($paciente->habito->alcohol == 1)
                            Si
                        @else
                            No
                        @endif
                        <br>
                        <strong>Otros: </strong>{{$paciente->habito->otro}}
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-6 invoice-col">
                        <strong>Número de Cigarillos Diarios: </strong>{{$paciente->habito->nr_cigarrillo}}<br>
                        <strong>Observacion: </strong>{{$paciente->habito->observacion}}<br>
                        <strong>Medicamentos: </strong>{{$paciente->habito->medicamento}}
                    </div>
                    <!-- /.col -->
                </div>
                <br>
                <table class="table table-bordered">
                    <tbody>
                        <tr>
                            <td><strong>Sospecha Diagnostica:</strong></td>
                            <td>{{$referenciaOdontologica->sospecha_diagnostica}}</td>
                        </tr>
                        <tr>
                            <td><strong>Motivo de Consulta:</strong></td>
                            <td>{{$referenciaOdontologica->motivo_consulta}}</td>
                        </tr>
                        <tr>
                            <td><strong>Antecedentes Mórbicos:</strong></td>
                            <td>{{$referenciaOdontologica->antecedente_morbido}}</td>
                        </tr>
                        <tr>
                            <td><strong>Alergias:</strong></td>
                            <td>{{$referenciaOdontologica->alergia}}</td>
                        </tr>
                        <tr>
                            <td><strong>Tamaño de Lesión:</strong></td>
                            <td>{{$referenciaOdontologica->tamano_lesion}} (mm)</td>
                        </tr>
                        <tr>
                            <td><strong>Tiempo de Evolución:</strong></td>
                            <td>{{$referenciaOdontologica->tm_evolucion}}  {{$referenciaOdontologica->unidad_tiempo->nombre}}</td>
                        </tr>
                        <tr>
                            <td><strong>Sintomatología:</strong></td>
                            <td>{{$referenciaOdontologica->sintomatologia}}</td>
                        </tr>
                        <tr>
                            <td><strong>Otros (Forma, Color, Consistencia, Etc):</strong></td>
                            <td>{{$referenciaOdontologica->otro}}</td>
                        </tr>
                        <tr>
                            <td><strong>Lesión Mucosa:</strong></td>
                            <td>{{$referenciaOdontologica->mucosas->pluck('id')->implode(', ')}}</td>
                        </tr>
                        <tr>
                            <td><strong>Lesión Ósea:</strong></td>
                            <td>{{$referenciaOdontologica->oseas->pluck('id')->implode(', ')}}</td>
                        </tr>
                    </tbody>
                </table>
                <br>
                <div class="row invoice-info">
                    <div class="col-sm-6 invoice-col">
                        <strong>Profesional: </strong>{{$referenciaOdontologica->profesional->nombre_completo}}<br>
                        <strong>Rut: </strong>{{$referenciaOdontologica->profesional->rut}}<br>
                        <strong>Fecha Solicitud: </strong>{{date('d/m/Y', strtotime(str_replace("/",".",$referenciaOdontologica->created_at)))}}
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-6 invoice-col">
                        <strong>Establecimiento: </strong>{{$referenciaOdontologica->establecimiento->tx_descripcion}}<br>
                        <strong>Comuna: </strong>{{$referenciaOdontologica->establecimiento->comuna->tx_descripcion}}
                    </div>
                    <!-- /.col -->
                </div>
            </div>
        </div>
              <!-- /.modal-content -->
    </div>
            <!-- /.modal-dialog -->
</div>