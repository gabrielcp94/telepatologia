<!DOCTYPE html>
<html lang="es">
    <head>
        <link rel="stylesheet" href="{{url('/')}}/css/base.css" />
        <link rel="stylesheet" href="{{url('/')}}/css/bootstrap.min.css" />
        <style type="text/css">
			table.table-bordered{ font-size: 11px; }
			table.table-bordered td{ padding: 3px;border-color: #909090!important;color: #000000!important;}
			table.table-bordered th{ padding: 3px;border-color: #909090!important;}
			.table tr td{
				font-size: 11px;
                /* LINE-HEIGHT:1px; */
			}
			.justify{
				text-align: justify;
			}
			.table-responsive{
				page-break-inside: avoid;
			}
            h3{
                text-align: center;
                text-transform: uppercase;
            }
            .texto{
                font-size: 11px;
            }
            .texto2{
                font-size: 11px;
                LINE-HEIGHT:0px;
            }
		</style>
        <meta charset="UTF-8">
        <title>Contra Referencia Telepatología Oral</title>
    </head>
    <body>
        <div class="col-xs-12 padding-0" >
			<table class="texto2" width="100%">
				<tr colspan="12">
					<td width="5%">
						<img style="max-width: 40px;" src="{{url('/')}}/images/logo.png" >
					</td>
					<td width="100%">
						<p>Servicio Salud Metropolitano Occidente</p>
                        <p>Hospital San Juan de Dios - CDT</p>
                    </td>	
                    <td width="10%">
                        <h3>{{$today}}</h3>
                    </td>			
                </tr>
            </table>
        </div>
        <h3>Contra Referencia Telepatología Oral</h3>
        <div class="table-responsive" >
            <table class="table table-bordered " width="100%">
                <tr>
                    <td colspan="6" align="center" style="background-color: #999999;" >
                        <strong>DATOS PERSONALES</strong>
                    </td>
                </tr>
                <tr>
                    <td width="10%" style="background-color: #eeeeee;">
                        <strong>Nombre</strong>
                    </td>
                    <td width="35%" >
                        {{$paciente->nombre_completo}}
                    </td>
                    <td width="15%" style="background-color: #eeeeee;">
                        <strong>F. de Nacimiento</strong>
                    </td>
                    <td width="15%" >
                        {{date('d/m/Y', strtotime(str_replace("/",".",$paciente->fc_nacimiento)))}}
                    </td>
                    <td width="10%" style="background-color: #eeeeee;">
                        <strong>Edad Actual</strong>
                    </td>
                    <td width="15%" >
                        {{$paciente->edad}}
                    </td>
                </tr>
                <tr>
                    <td style="background-color: #eeeeee;">
                        <strong>Rut</strong>
                    </td>
                    <td>
                        {{$paciente->rut}}
                    </td>
                    <td style="background-color: #eeeeee;">
                        <strong>Sexo</strong>
                    </td>
                    <td>
                        {{$paciente->sexo->tx_descripcion}}
                    </td>
                    <td style="background-color: #eeeeee;">
                        <strong>Previsión</strong>
                    </td>
                    <td>
                        {{$paciente->prevision->tx_descripcion}} {{$paciente->clasificacionFonasa->tx_descripcion}}
                    </td>
                </tr>
                <tr>
                    <td style="background-color: #eeeeee;">
                        <strong>Teléfono</strong>
                    </td>
                    <td>
                        {{$paciente->tx_telefono}}
                    </td>
                    <td style="background-color: #eeeeee;">
                        <strong>Dirección</strong>
                    </td>
                    <td colspan="3">
                        {{$paciente->tx_direccion}} ({{$paciente->comuna->tx_descripcion}})
                    </td>
                </tr>
            </table>
        </div>
        <br>
        <div class="table-responsive" >
            <table class="table table-bordered " width="100%">
                <tr>
                    <td colspan="6" align="center" style="background-color: #999999;" >
                        <strong>DATOS DE DERIVACIÓN</strong>
                    </td>
                </tr>
                <tr>
                    <td style="background-color: #eeeeee;">
                        <strong>Nombre</strong>
                    </td>
                    <td>
                        {{$referenciaOdontologica->profesional->nombre_completo}}
                    </td>
                    <td style="background-color: #eeeeee;">
                        <strong>Establecimiento</strong>
                    </td>
                    <td colspan="3">
                        {{$referenciaOdontologica->establecimiento->tx_descripcion}}
                    </td>
                </tr>
                <tr>
                    <td width="10%" style="background-color: #eeeeee;">
                        <strong>Rut</strong>
                    </td>
                    <td width="35%">
                        {{$referenciaOdontologica->profesional->rut}}
                    </td>
                    <td width="15%" style="background-color: #eeeeee;">
                        <strong>Comuna</strong>
                    </td>
                    <td width="15%">
                        {{$referenciaOdontologica->establecimiento->comuna->tx_descripcion}}
                    </td>
                    <td width="10%" style="background-color: #eeeeee;">
                        <strong>F. Solicitud</strong>
                    </td>
                    <td width="15%">
                        {{date('d/m/Y', strtotime(str_replace("/",".",$referenciaOdontologica->created_at)))}}
                    </td>
                </tr>
                <tr>
                    <td style="background-color: #eeeeee;">
                        <strong>Sospecha Diagnostica</strong>
                    </td>
                    <td>
                        {{$referenciaOdontologica->sospecha_diagnostica}}
                    </td>
                    <td style="background-color: #eeeeee;">
                        <strong>Diagnostico Cie10</strong>
                    </td>
                    <td>
                        {{$referenciaOdontologica->cie10->nombre_completo}}
                    </td>
                    <td style="background-color: #eeeeee;">
                        <strong>Sospecha de Cancer</strong>
                    </td>
                    <td>
                        {{$referenciaOdontologica->bo_cancer == 1 ? 'Si' : 'No'}}
                    </td>
                </tr>
            </table>
        </div>
        <br>
        <div class="table-responsive" >
            <table class="table table-bordered " width="100%">
                <tr>
                    <td colspan="6" align="center" style="background-color: #999999;" >
                        <strong>DATOS DE CONTRAREFERENTE</strong>
                    </td>
                </tr>
                <tr>
                    <td style="background-color: #eeeeee;">
                        <strong>Nombre</strong>
                    </td>
                    <td>
                        {{$referenciaOdontologica->contraReferencia->profesional->nombre_completo}}
                    </td>
                    <td style="background-color: #eeeeee;">
                        <strong>Establecimiento</strong>
                    </td>
                    <td colspan="3">
                        Hospital San Juan de Dios (Santiago, Santiago)
                    </td>
                </tr>
                <tr>
                    <td width="10%" style="background-color: #eeeeee;">
                        <strong>Rut</strong>
                    </td>
                    <td width="35%">
                        {{$referenciaOdontologica->contraReferencia->profesional->rut}}
                    </td>
                    <td width="15%" style="background-color: #eeeeee;">
                        <strong>Comuna</strong>
                    </td>
                    <td width="15%">
                        SANTIAGO
                    </td>
                    <td width="10%" style="background-color: #eeeeee;">
                        <strong>F. Respuesta</strong>
                    </td>
                    <td width="15%">
                        {{date('d/m/Y', strtotime(str_replace("/",".",$referenciaOdontologica->contraReferencia->created_at)))}}
                    </td>
                </tr>
            </table>
        </div>
        <br>
        <div class="table-responsive" >
            <table class="table table-bordered " width="100%">
                <tr>
                    <td colspan="6" align="center" style="background-color: #999999;" >
                        <strong>PROCEDIMIENTO A SEGUIR</strong>
                    </td>
                </tr>
                <tr>
                    <td width="20%" style="background-color: #eeeeee;">
                        <strong>Prioridad para la especialidad</strong>
                    </td>
                    <td width="40%" >
                        {{($referenciaOdontologica->contraReferencia->urgente == 1) ? 'URGENTE' : 'NO URGENTE'}}
                    </td>
                    <td width="30%" style="background-color: #eeeeee;">
                        <strong>Control en APS por odontólogo tratante</strong>
                    </td>
                    <td width="10%" >
                        {{($referenciaOdontologica->contraReferencia->control_aps == 1) ? 'SI' : 'NO'}}
                    </td>
                </tr>
                <tr>
                    <td style="background-color: #eeeeee;">
                        <strong>Causal Número</strong>
                    </td>
                    <td>
                        {{$referenciaOdontologica->contraReferencia->causalNumero->nombre}}
                    </td>
                    <td style="background-color: #eeeeee;">
                        <strong>Control en HSJD por especialista (Patología oral)</strong>
                    </td>
                    <td>
                        {{($referenciaOdontologica->contraReferencia->control_hsjd == 1) ? 'SI' : 'NO'}}
                    </td>
                </tr>
                <tr>
                    <td style="background-color: #eeeeee;">
                        <strong>Derivación no pertinente o a otra especialidad</strong>
                    </td>
                    <td>
                        {{($referenciaOdontologica->contraReferencia->derivacion == 1) ? 'PERTINENTE' : 'NO PERTINENTE'}} ({{isset($referenciaOdontologica->contraReferencia->motivoNoPertinente->nombre) ? $referenciaOdontologica->contraReferencia->motivoNoPertinente->nombre : ''}})
                    </td>
                    <td style="background-color: #eeeeee;">
                        <strong>Egreso en APS</strong>
                    </td>
                    <td>
                        {{($referenciaOdontologica->contraReferencia->egreso_aps == 1) ? 'SI' : 'NO'}}
                    </td>
                </tr>
                <tr>
                    <td style="background-color: #eeeeee;">
                        <strong>Indicaciones u observaciones</strong>
                    </td>
                    <td>
                        {{$referenciaOdontologica->contraReferencia->observacion}}
                    </td>
                </tr>
            </table>
        </div>
        <br>
        <br>
        <div class="texto" align="right">
            __________________________________<br><br>
            {{$referenciaOdontologica->contraReferencia->profesional->nombre_completo}}<br>
            {{$referenciaOdontologica->contraReferencia->profesional->rut}}
        </div>
    </body>
</html>