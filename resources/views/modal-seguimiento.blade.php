<div class="modal fade" id="modal-seguimiento" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Nuevo Seguimiento ({{$referenciaOdontologica->id}})</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            {{-- <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Referencia Odontologica ({{$itemReferenciaOdontologica->id}})</h4>
            </div> --}}
            <div class="modal-body">
                <div class="row invoice-info">
                    <div class="col-sm-6 invoice-col">
                        <strong>Nombre: </strong>{{$paciente->nombre_completo}}<br>
                        <strong>Rut: </strong>{{$paciente->rut}}<br>
                        <strong>Fecha de Nacimiento: </strong>
                        @if($paciente->fc_nacimiento != '0000-00-00')
                            {{date('d/m/Y', strtotime(str_replace("/",".",$paciente->fc_nacimiento)))}}
                        @else 
                            Sin Información
                        @endif<br>
                        <strong>Previsión: </strong>{{$paciente->prevision->tx_descripcion}} {{$paciente->clasificacionFonasa->tx_descripcion}}
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-6 invoice-col">
                        <strong>Sexo: </strong>{{$paciente->sexo->tx_descripcion}}<br>
                        <strong>Edad: </strong>{{$paciente->edad}}<br>
                        <strong>Dirección: </strong>{{$paciente->tx_direccion}} ({{$paciente->comuna->tx_descripcion}})<br>
                        <strong>Telefono: </strong>
                        @if(isset($paciente->tx_telefono))
                            {{$paciente->tx_telefono}}
                        @else
                            Sin Información
                        @endif
                    </div>
                    <!-- /.col -->
                </div>
                @if(!$referenciaOdontologica->seguimientos->isEmpty())
                    <br>
                    <div class="col-sm-6 invoice-col">
                        <strong>Seguimiento:</strong><br>
                    </div>
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <td><strong>Fecha:</strong></td>
                                <td><strong>Comentario</strong></td>
                            </tr>
                            @foreach ($referenciaOdontologica->seguimientos->sortByDesc('fc') as $seguimiento)
                                <tr>
                                    <td>{{date('d/m/Y', strtotime(str_replace("/",".",$seguimiento->fc)))}}</td>
                                    <td>{{$seguimiento->comentario}}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                @endif
                <form role="form" class="form-horizontal" id="form" method="POST" action="{{action('ReferenciaOdontologicaController@storeSeguimiento')}}">
                    {{ csrf_field() }}
                    <input type="hidden" id="id_referencia_odontologica" name="id_referencia_odontologica" value="{{$referenciaOdontologica->id}}">	
                    <div class="form-group">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-4">
                                    <label for="fc">Fecha</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                        </div>
                                        <input type="date" class="form-control" id="fc" name="fc" required>
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <label for="comentario">Comentario</label>
                                    <textarea class="form-control noresize" id="comentario" name="comentario" required></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="text-right">
                        <button type="submit" class="btn btn-info">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
              <!-- /.modal-content -->
    </div>
            <!-- /.modal-dialog -->
</div>