<!DOCTYPE html>
<html lang="es">
    <head>
        <link rel="stylesheet" href="{{url('/')}}/css/base.css" />
        <link rel="stylesheet" href="{{url('/')}}/css/bootstrap.min.css" />
        <style type="text/css">
			table.table-bordered{ font-size: 11px; }
			table.table-bordered td{ padding: 3px;border-color: #909090!important;color: #000000!important;}
			table.table-bordered th{ padding: 3px;border-color: #909090!important;}
			.table tr td{
				font-size: 11px;
                /* LINE-HEIGHT:1px; */
			}
			.justify{
				text-align: justify;
			}
			.table-responsive{
				page-break-inside: avoid;
			}
            h3{
                text-align: center;
                text-transform: uppercase;
            }
            .texto{
                font-size: 11px;
            }
            .texto2{
                font-size: 11px;
                LINE-HEIGHT:0px;
            }
		</style>
        <meta charset="UTF-8">
        <title>Referencia Telepatología Oral</title>
    </head>
    <body>
        <div class="col-xs-12 padding-0" >
			<table class="texto2" width="100%">
				<tr colspan="12">
					<td width="5%">
						<img style="max-width: 40px;" src="{{url('/')}}/images/logo.png" >
					</td>
					<td width="100%">
						<p>Servicio Salud Metropolitano Occidente</p>
                        <p>Hospital San Juan de Dios - CDT</p>
                    </td>	
                    <td width="10%">
                        <h3>{{$today}}</h3>
                    </td>			
                </tr>
            </table>
        </div>
        <h3>Referencia Telepatología Oral</h3>
        <div class="table-responsive" >
            <table class="table table-bordered " width="100%">
                <tr>
                    <td colspan="6" align="center" style="background-color: #999999;" >
                        <strong>DATOS PERSONALES</strong>
                    </td>
                </tr>
                <tr>
                    <td width="10%" style="background-color: #eeeeee;">
                        <strong>Nombre</strong>
                    </td>
                    <td width="35%" >
                        {{$paciente->nombre_completo}}
                    </td>
                    <td width="15%" style="background-color: #eeeeee;">
                        <strong>F. de Nacimiento</strong>
                    </td>
                    <td width="15%" >
                        {{date('d/m/Y', strtotime(str_replace("/",".",$paciente->fc_nacimiento)))}}
                    </td>
                    <td width="10%" style="background-color: #eeeeee;">
                        <strong>Edad</strong>
                    </td>
                    <td width="15%" >
                        {{$paciente->edad}}
                    </td>
                </tr>
                <tr>
                    <td style="background-color: #eeeeee;">
                        <strong>Rut</strong>
                    </td>
                    <td>
                        {{$paciente->rut}}
                    </td>
                    <td style="background-color: #eeeeee;">
                        <strong>Sexo</strong>
                    </td>
                    <td>
                        {{$paciente->sexo->tx_descripcion}}
                    </td>
                    <td style="background-color: #eeeeee;">
                        <strong>Previsión</strong>
                    </td>
                    <td>
                        {{$paciente->prevision->tx_descripcion}} {{$paciente->clasificacionFonasa->tx_descripcion}}
                    </td>
                </tr>
                <tr>
                    <td style="background-color: #eeeeee;">
                        <strong>Teléfono</strong>
                    </td>
                    <td>
                        {{$paciente->tx_telefono}}
                    </td>
                    <td style="background-color: #eeeeee;">
                        <strong>Dirección</strong>
                    </td>
                    <td colspan="3">
                        {{$paciente->tx_direccion}} ({{$paciente->comuna->tx_descripcion}})
                    </td>
                </tr>
            </table>
        </div>
        <br>
        <div class="table-responsive" >
            <table class="table table-bordered " width="100%">
                <tr>
                    <td colspan="6" align="center" style="background-color: #999999;" >
                        <strong>DATOS DE DERIVACIÓN</strong>
                    </td>
                </tr>
                <tr>
                    <td style="background-color: #eeeeee;">
                        <strong>Nombre</strong>
                    </td>
                    <td>
                        {{$referenciaOdontologica->profesional->nombre_completo}}
                    </td>
                    <td style="background-color: #eeeeee;">
                        <strong>Establecimiento</strong>
                    </td>
                    <td colspan="3">
                        {{$referenciaOdontologica->establecimiento->tx_descripcion}}
                    </td>
                </tr>
                <tr>
                    <td width="10%" style="background-color: #eeeeee;">
                        <strong>Rut</strong>
                    </td>
                    <td width="35%">
                        {{$referenciaOdontologica->profesional->rut}}
                    </td>
                    <td width="15%" style="background-color: #eeeeee;">
                        <strong>Comuna</strong>
                    </td>
                    <td width="15%">
                        {{$referenciaOdontologica->establecimiento->comuna->tx_descripcion}}
                    </td>
                    <td width="10%" style="background-color: #eeeeee;">
                        <strong>F. Solicitud</strong>
                    </td>
                    <td width="15%">
                        {{date('d/m/Y', strtotime(str_replace("/",".",$referenciaOdontologica->created_at)))}}
                    </td>
                </tr>
            </table>
        </div>
        <br>
        <div class="table-responsive" >
            <table class="table table-bordered " width="100%">
                <tr>
                    <td colspan="6" align="center" style="background-color: #999999;" >
                        <strong>ANTECEDENTES CLINICOS</strong>
                    </td>
                </tr>
                <tr>
                    <td width="15%" style="background-color: #eeeeee;">
                        <strong>Diagnostico Cie10</strong>
                    </td>
                    <td width="30%" >
                        {{$referenciaOdontologica->cie10->nombre_completo}}
                    </td>
                    <td width="20%" style="background-color: #eeeeee;">
                        <strong>Sospecha Diagnostica</strong>
                    </td>
                    <td width="35%" >
                        {{$referenciaOdontologica->sospecha_diagnostica}}
                    </td>
                </tr>
                <tr>
                    <td style="background-color: #eeeeee;">
                        <strong>Motivo de Consulta</strong>
                    </td>
                    <td>
                        {{$referenciaOdontologica->motivo_consulta}}
                    </td>
                    <td style="background-color: #eeeeee;">
                        <strong>Antecedentes Mórbicos</strong>
                    </td>
                    <td>
                        {{$referenciaOdontologica->antecedente_morbido}}
                    </td>
                </tr>
                <tr>
                    <td style="background-color: #eeeeee;">
                        <strong>Alergias</strong>
                    </td>
                    <td>
                        {{$referenciaOdontologica->alergia}}
                    </td>
                    <td style="background-color: #eeeeee;">
                        <strong>Sospecha de Cancer</strong>
                    </td>
                    <td>
                        {{$referenciaOdontologica->bo_cancer == 1 ? 'Si' : 'No'}}
                    </td>
                </tr>
            </table>
        </div>
        <br>
        <div class="table-responsive" >
            <table class="table table-bordered " width="100%">
                <tr>
                    <td colspan="4" align="center" style="background-color: #999999;" >
                        <strong>HÁBITO</strong>
                    </td>
                </tr>
                <tr>
                    <td width="10%" style="background-color: #eeeeee;">
                        <strong>Fumador</strong>
                    </td>
                    <td width="35%" >
                        @if($paciente->habito->nr_cigarrillo > 0)
                            Si, {{$paciente->habito->nr_cigarrillo}} cigarrillos diarios.
                        @else
                            No.
                        @endif
                    </td>
                    <td width="20%" style="background-color: #eeeeee;">
                        <strong>Consumo de Alcohol</strong>
                    </td>
                    <td width="35%" >
                        @if($paciente->habito->alcohol == 1)
                            Si. {{$paciente->habito->observacion}}
                        @else
                            No.
                        @endif
                    </td>
                </tr>
                <tr>
                    <td style="background-color: #eeeeee;">
                        <strong>Otros</strong>
                    </td>
                    <td>
                        {{$paciente->habito->otro}}
                    </td>
                    <td style="background-color: #eeeeee;">
                        <strong>Medicamentos</strong>
                    </td>
                    <td>
                        {{$paciente->habito->medicamento}}
                    </td>
                </tr>
            </table>
        </div>
        <br>
        <div class="table-responsive" >
            <table class="table table-bordered " width="100%">
                <tr>
                    <td colspan="4" align="center" style="background-color: #999999;" >
                        <strong>DESCRIPCIÓN DE LA LESIÓN</strong>
                    </td>
                </tr>
                <tr>
                    <td width="15%" style="background-color: #eeeeee;">
                        <strong>Tamaño de la lesión</strong>
                    </td>
                    <td width="30%">
                        {{$referenciaOdontologica->tamano_lesion}}
                    </td>
                    <td width="20%" style="background-color: #eeeeee;">
                        <strong>Tiempo de evolución</strong>
                    </td>
                    <td width="35%">
                        {{$referenciaOdontologica->tm_evolucion}} {{$referenciaOdontologica->unidad_tiempo->nombre}}
                    </td>
                </tr>
                <tr>
                    <td style="background-color: #eeeeee;">
                        <strong>Sintomatología</strong>
                    </td>
                    <td>
                        {{$referenciaOdontologica->sintomatologia}}
                    </td>
                    <td style="background-color: #eeeeee;">
                        <strong>Otros (Forma, color, consistencia, etc)</strong>
                    </td>
                    <td>
                        {{$referenciaOdontologica->otro}}
                    </td>
                </tr>
            </table>
        </div>
        <br>
        <div class="table-responsive" >
            <table class="table table-bordered " >
                <tr>
                    <td colspan="4" align="center" style="background-color: #999999;" >
                        <strong>UBICACIÓN</strong>
                    </td>
                </tr>
                <tr>
                    <td width="15%" style="background-color: #eeeeee;">
                        <strong>Lesión Mucosa</strong>
                    </td>
                    <td width="35%">
                        {{$referenciaOdontologica->mucosas->pluck('id')->implode(', ')}}
                    </td>
                    <td width="15%" style="background-color: #eeeeee;">
                        <strong>Lesión Osea</strong>
                    </td>
                    <td width="35%">
                        {{$referenciaOdontologica->oseas->pluck('id')->implode(', ')}}
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="background-color: #eeeeee;">
                        <img style="width:50%" src="{{ public_path('/images/mucosa.jpg') }}">
                    </td>
                    <td colspan="2" style="background-color: #eeeeee;">
                        <img style="width:50%" src="{{ public_path('/images/osea.png') }}">
                    </td>
                </tr>
            </table>
        </div>
        <br>
        <br>
        <div class="texto" align="right">
            __________________________________<br><br>
            {{$referenciaOdontologica->profesional->nombre_completo}}<br>
            {{$referenciaOdontologica->profesional->rut}}
        </div>
    </body>
</html>