<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title>Revocación del Consentimiento Informado</title>
        <style>
        h1{
        text-align: center;
        text-transform: uppercase;
        }
        .contenido{
        font-size: 14px;
        }
        #primero{
        background-color: #ccc;
        }
        #segundo{
        color:#44a359;
        }
        #tercero{
        text-decoration:line-through;
        }
    </style>
    </head>
    <body>
        <h2 align='center'>Revocacion del Consentimiento Informado</h2>
        <hr>
        <br>
        Yo, <u><strong>{{$paciente->nombre_completo}}</strong></u> después de ser informado sobre la atención por Telemedicina, 
        de sus ventajas y riesgos y comprendiendo las posibles consecuencias de no hacerlo, manifiesto mi decisión de retirar 
        mi autorización para la realización de dicha atención.
        <br>
        Este documento no modifica la calidad de la atención que recibiré en el establecimiento
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <strong>PACIENTE O REPRESENTANTE DEL PACIENTE</strong>
        <br>
        NOMBRE: _________________________________________
        <br>
        RUT: _____________________________________________
        <br>
        FIRMA: ___________________________________________
        <br>
        <br>
        <br>
        <br>
        <br>
        <strong>UNA VEZ FIRMADO ESTE DOCUMENTO DEBE QUEDAR INCORPORADO A LA FICHA CLÍNICA DEL PACIENTE.</strong>
    </body>
</html>