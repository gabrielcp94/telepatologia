<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title>Consentimiento Informado</title>
        <style>
        h1{
        text-align: center;
        text-transform: uppercase;
        }
        .contenido{
        font-size: 14px;
        }
        #primero{
        background-color: #ccc;
        }
        #segundo{
        color:#44a359;
        }
        #tercero{
        text-decoration:line-through;
        }
    </style>
    </head>
    <body>
        <h2 align='center'>Consentimiento Informado</h2>
        <hr>
        <br>
        El Odontólogo <u><strong>{{$referenciaOdontologica->profesional->nombre_completo}}</strong></u> me ha explicado 
        lo que significa ser atendido por Telemedicina y su propósito. He sido informado de las ventajas, complicaciones, 
        posibles alternativas y riesgos. He comprendido el significado de la atención y sus riesgos. Se me ha dado la 
        oportunidad de hacer preguntas y ampliar la información recibida. Me han informado que tengo la libertad de rechazar 
        la atención por Telemedicina y/o de revocar mi consentimiento en cualquier momento antes de que realice esta atención, 
        sin que ello altere la calidad de la atención que reciba. Por lo tanto, mi decisión tomada libre y conscientemente es:
        <br>
        <br>
        SI__ / NO __ Autorizo a que me realice la atención de interconsulta virtual.
        <br>
        <br>
        SI__ / NO __ Autorizo a la toma y posterior envío de imagen de mi lesión para ser evaluada por especialista en Hospital 
        San Juan de Dios.
        <br>
        <br>
        NOTA: Marque con una cruz el casillero de su elección.
        <br>
        <br>
        <br>
        En Santiago a <u>{{$today}}.</u>
        <br>
        <br>
        <br>
        <strong>PACIENTE O REPRESENTANTE DEL PACIENTE</strong>
        <br>
        NOMBRE: {{$referenciaOdontologica->paciente->nombre_completo}}
        <br>
        RUT: {{$referenciaOdontologica->paciente->rut}}
        <br>
        FIRMA: ___________________________________________
        <br>
        <br>
        <br>
        NOTA: Sólo en caso de compromiso de conciencia del paciente o dificultad de entendimiento, puede firmar este 
        consentimiento, un representante del paciente.
        <br>
        <br>
        <strong>UNA VEZ FIRMADO ESTE DOCUMENTO DEBE QUEDAR INCORPORADO A LA FICHA CLÍNICA DEL PACIENTE.</strong>
    </body>
</html>