@extends('adminlte::page')

@section('title', 'Paciente')

@section('content')
	<div class="invoice p-3 mb-3">
		@if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        @endif
        @if(session()->has('error'))
            <div class="alert alert-danger">
               <i class="fa fa-exclamation-triangle"></i> {{ session()->get('error') }}
            </div>
        @endif
        <!-- title row -->
        <div class="row">
			<div class="col-12">
				<h4>
					<i class="fas fa-user"></i> Paciente
				</h4>
				<hr>
			</div>
			<!-- /.col -->
        </div>
              <!-- info row -->
		<div class="row invoice-info">
			<div class="col-sm-6 invoice-col">
				<strong>Nombre: </strong>{{$paciente->nombre_completo}}<br>
				@if($paciente->id_tipo_identificacion_paciente == 1)
					<strong>Rut: </strong>{{$paciente->rut}}<br>
				@else
					<strong>Pasaporte: </strong>{{$paciente->tx_pasaporte}}<br>
				@endif
				<strong>Fecha de Nacimiento: </strong>
				@if($paciente->fc_nacimiento != '0000-00-00')
					{{date('d/m/Y', strtotime(str_replace("/",".",$paciente->fc_nacimiento)))}}
				@else 
					Sin Información
				@endif<br>
				<strong>Previsión: </strong>{{$paciente->prevision->tx_descripcion}} {{$paciente->clasificacionFonasa->tx_descripcion}}
			</div>
			<!-- /.col -->
			<div class="col-sm-6 invoice-col">
				<strong>Sexo: </strong>{{$paciente->sexo->tx_descripcion}}<br>
				<strong>Edad: </strong>{{$paciente->edad}}<br>
				<strong>Dirección: </strong>{{$paciente->tx_direccion}} ({{$paciente->comuna->tx_descripcion}})<br>
				<strong>Telefono: </strong>
				@if(isset($paciente->tx_telefono))
					{{$paciente->tx_telefono}}
				@else
					Sin Información
				@endif
			</div>
			<!-- /.col -->
		</div>
		<br>
		<!-- /.row -->

		<!-- Table row -->
		<div class="row">
			<div class="col-12 table-responsive">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>ID</th>
							<th>Sospecha Diagnóstica</th>
							<th>Establecimiento</th>
							<th>Fecha Solicitud</th>
							<th>Profesional</th>
							<th>Estado</th>
							<th><i class="fas fa-cog"></i></th>
						</tr>
					</thead>
					<tbody>
						@foreach ($paciente->referenciasOdontologicas->sortByDesc('created_at') as $referenciaOdontologica)
							@php
								$urlDeleteReferenciaOdontologica = url('deleteReferenciaOdontologica/'.$referenciaOdontologica->id);
							@endphp 
							@if((Auth::user()->id_perfil == 3 && Auth::user()->id == $referenciaOdontologica->created_by) || (Auth::user()->id_perfil == 4 && $referenciaOdontologica->id_estado != 0) || Auth::user()->id_perfil == 1)
								<tr>
									<td>{{$referenciaOdontologica->id}}</td>
									<td>{{$referenciaOdontologica->sospecha_diagnostica}}</td>
									<td>{{$referenciaOdontologica->establecimiento->tx_descripcion}}</td>
									<td>{{date('d/m/Y', strtotime(str_replace("/",".",$referenciaOdontologica->created_at)))}}</td>
									<td>{{$referenciaOdontologica->profesional->nombre_completo}}</td>
									<td><small class="badge badge-{{$referenciaOdontologica->estado->clase}}">{{$referenciaOdontologica->estado->nombre}}</small>
										@if($referenciaOdontologica->bo_cancer == 1)
											<small class="badge badge-danger">S. Cancer</small>
										@endif
									</td>
									<td><div class="btn-group">
										<a onclick="detalle({{$referenciaOdontologica->id}})" type="button" title="Ver Detalle" class="btn btn-info btn-sm"><i class="fa fa-file" style="color:white"></i></a>
										@if($referenciaOdontologica->id_estado == 0)
											@if($referenciaOdontologica->consentimiento == null)
												<a href="pdfConsentimientoInformado?id={{$referenciaOdontologica->id}}" title="Descargar Consentimiento" class="btn btn-success btn-sm"><i class="fa fa-download" style="color:white"></i></a>
											@endif
											<a href="referenciaOdontologica/create?id={{$referenciaOdontologica->id}}" title="Terminar Referencia" class="btn btn-warning btn-sm"><i class="fa fa-book-medical" style="color:white"></i></a>
										@endif
										@if($referenciaOdontologica->id_estado != 0)
											<a href="pdfReferenciaOdontologica?id={{$referenciaOdontologica->id}}" target="_blank" title="Referencia Odontologica en PDF" class="btn btn-success btn-sm"><i class="fa fa-book-medical" style="color:white"></i></a>
										@endif
										@if(isset($referenciaOdontologica->consentimiento))
											<a href="storage/{{$referenciaOdontologica->consentimiento}}" target="_blank" title="Consentimiento Informado" class="btn btn-success btn-sm"><i class="fa fa-check" style="color:white"></i></a>
										@endif
										@if(isset($referenciaOdontologica->contraReferencia) && in_array($referenciaOdontologica->id_estado, [3,4]))
											<a href="pdfContraReferencia?id={{$referenciaOdontologica->id}}" target="_blank" title="Contra Referencia PDF" class="btn btn-success btn-sm"><i class="fa fa-notes-medical" style="color:white"></i></a>
										@else
											@if(in_array(Auth::user()->id_perfil, [1,4]))
												<a href="contraReferencia/create?id={{$referenciaOdontologica->id}}" title="Gestionar Solicitud" class="btn btn-warning btn-sm"><i class="fa fa-notes-medical" style="color:white"></i></a>
											@endif
										@endif
										@if($referenciaOdontologica->id_estado == 0)
											<a onclick="eliminar('{{$urlDeleteReferenciaOdontologica}}');" title="Eliminar Referencia Odontologica" type="button" class="btn btn-danger btn-sm"><i class="fa fa-trash" style="color:white"></i></a>
										@endif
										@if($referenciaOdontologica->id_estado == 3 && in_array(Auth::user()->id_perfil, [1,3]))
											<a onclick="seguimiento({{$referenciaOdontologica->id}})" type="button" title="Agregar Seguimiento" class="btn btn-warning btn-sm"><i class="fa fa-comment-medical" style="color:white"></i></a>
										@endif
									</div></td>
								</tr>
							@endif
						@endforeach
					</tbody>
				</table>
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</div>
	<div id="modal"></div> 
@stop

@section('js')
<script>
	function detalle(id)
	{
		$.get( '{{ url("referenciaOdontologicaDetalle") }}/' + id, function( data ) {
			$( "#modal" ).html( data );
			$( "#modal-detalle" ).modal();
		});
	};

	function consentimiento(id)
	{
		$.get( '{{ url("referenciaOdontologicaConsentimiento") }}/' + id, function( data ) {
			$( "#modal" ).html( data );
			$( "#modal-consentimiento" ).modal();
		});
	};

	function revocacion(id)
	{
		$.get( '{{ url("referenciaOdontologicaRevocacion") }}/' + id, function( data ) {
			$( "#modal" ).html( data );
			$( "#modal-revocacion" ).modal();
		});
	};

	function eliminar(url)
    {
        var opcion = confirm("Estas seguro de eliminar esta Referencia Odontologica?");
        if (opcion == true) {
            location.href = url;
        } else {
            return false;
        }
	}
	
	function seguimiento(id)
	{
		$.get( '{{ url("referenciaOdontologicaSeguimiento") }}/' + id, function( data ) {
			$( "#modal" ).html( data );
			$( "#modal-seguimiento" ).modal();
		});
	};

	$(".alert-success").fadeTo(20000, 500).slideUp(500, function(){
        $(".alert-success").slideUp(1000);
    });

    $(".alert-danger").fadeTo(20000, 5000).slideUp(500, function(){
        $(".alert-danger").slideUp(1000);
    });
</script>
@endsection


