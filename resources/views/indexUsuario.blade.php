@extends('adminlte::page')

@section('title', 'Lista de Usuarios')

@section('content_header')
    @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @endif
    @if(session()->has('error'))
        <div class="alert alert-danger">
            <i class="fa fa-exclamation-triangle"></i> {{ session()->get('error') }}
        </div>
    @endif
@stop

@section('content')
		{{-- <form class="form-horizontal" id="form" method="POST">
			{{ csrf_field() }} --}}
			

	<div class="card card-info">
		<div class="card-header">
		<h3 class="card-title">Lista de Usuarios</h3>
		</div>
		<!-- /.card-header -->
		<!-- form start -->
		<div class="card-body">
            <div id="example1_wrapper" class="dataTables_wrapper dt-bootstrap4">
                <div class="row">
                    <div class="col-sm-12">
                        <form class="form-horizontal">
                            <div class="form-group row">
                                <div class="col-sm-5">
                                    <input type="text" name="nombre" class="form-control" id="nombre" placeholder="Nombre" value="{{request()->nombre}}">
                                </div>
                                <div class="col-sm-3">
                                    <input type="text" name="rut" class="form-control" id="rut" placeholder="Rut" value="{{request()->rut}}">
                                </div>
                                <div class="col-sm-2">
                                    <button type="submit" class="btn btn-info">Filtrar</button>
                                </div>
                                <div class="col-sm-2">
                                    <a href="usuario/create" class="btn btn-success" type="button" title="Agregar Usuario"><i class="fa fa-user-plus" style="color:white"></i></a>
                                </div>
                            </div>
                        </form>
                        <table class="table table-striped table-hover" id="tableUsers">
                            <thead>
                                <th>ID</th>
                                <th>Nombre</th>
                                <th>Rut</th>
                                <th>Email</th>
                                <th>Perfil</th>
                                <th><i class="fa fa-cog"></i></th>
                            </thead>
                            <tbody>
                                @foreach ($usuarios as $usuario)
                                    @php
                                        $urlDeleteUsuario = url('deleteUsuario/'.$usuario->id);
                                        $urlResetUsuario = url('resetUsuario/'.$usuario->id);
                                    @endphp 
                                    <tr role="row" class="odd">
                                        <td>{{$usuario->id}}</td>
										<td>{{$usuario->nombre_completo}}</td> 
                                        <td>{{$usuario->rut}}</td>
                                        <td>{{$usuario->tx_email}}</td>
                                        <td>{{$usuario->perfil->nombre}}</td>
										<td><div class="btn-group">
                                            @if(Auth::user()->id_perfil == 1 || $usuario->id_perfil > 2 || Auth::user()->id == $usuario->id)
                                                <a href="editUsuario/{{$usuario->id}}" title="Editar Usuario" class="btn btn-warning btn-sm"><i class="fa fa-edit" style="color:white"></i></a>
                                                <a onclick="reset('{{$urlResetUsuario}}');" title="Reiniciar Contraseña" type="button" class="btn btn-primary btn-sm"><i class="fa fa-lock-open" style="color:white"></i></a>
                                                <a onclick="eliminar('{{$urlDeleteUsuario}}');" title="Eliminar Usuario" type="button" class="btn btn-danger btn-sm"><i class="fa fa-trash" style="color:white"></i></a>
                                            @endif
										</div></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ $usuarios->links() }}
                    </div>
                </div>
            </div>
        </div>
        <!-- /.card-body -->
    </div>
@stop

@section('js')
<script>
    function reset(url)
    {
        var opcion = confirm("Estas seguro de reiniciar la contraseña de este usuario?");
        if (opcion == true) {
            location.href = url;
        } else {
            return false;
        }
    }

    function eliminar(url)
    {
        var opcion = confirm("Estas seguro de eliminar este usuario?");
        if (opcion == true) {
            location.href = url;
        } else {
            return false;
        }
    }

	$(".alert-success").fadeTo(20000, 500).slideUp(500, function(){
        $(".alert-success").slideUp(1000);
    });

    $(".alert-danger").fadeTo(20000, 5000).slideUp(500, function(){
        $(".alert-danger").slideUp(1000);
    });
</script>
@stop